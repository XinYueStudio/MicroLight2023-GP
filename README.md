# MicroLight2023-GP
MicroLight2023-物探

Unity 版本：2021.3.9f1

 
## 更新日期：2023-08-05
 

 1、放置资源文件
 Assets\MicroLight2023_GP\mapData
 ![1691209592321](https://github.com/XinYueStudio/MicroLight2023-GP/assets/15990821/418eb6c8-63bf-4598-be1f-221fa665298f)

 2、打开样例场景
 Assets\Scenes\SampleScene.unity
 ![1691209876022](https://github.com/XinYueStudio/MicroLight2023-GP/assets/15990821/2c2dafe6-d493-4ebd-8fe0-751afce67ac4)

 3、全息主机连接一个外接显示器设置为主屏幕,全息投影仪设置为副屏

 4、设置全息主机为左右3D模式

 5、运行调试时是否开机定位追踪方法（图中是不开机定位追踪，默认自定义位置）
 ![image](https://github.com/XinYueStudio/MicroLight2023-GP/assets/15990821/0c8975ef-6cb8-454c-8dbb-6457da9c26d0)

 6、使用Unity Terrain Tool 导入 tif 后需要手动设置 Terrain 的长宽高以及设置Terrain Layer 配置纹理

 7、tif  Terrain 导入完成后 需要调整 MicriLight 全息SDK 虚拟桌面的位置姿态 ，设置到合适的位置

 8、勘探探针柱子 需要自定义模型引入和布局

 9、勘探车辆运行布局也需要自定义引入和布局

 

 ## 更新日期：2023-08-12

 1、新增TerrainBoundsGetCorners 计算地型 8顶角

 2、新增ExplorationPointsLoader 加载勘探转井文件，和生成样例文件

 3、样例文件在：Assets\StreamingAssets\勘探转井文件.json

 ![image](/41]GIP[\)D51V~Z57Y@K17VD.png)
 

 ## 更新日期：2023-08-29

 
 1、QuakePointsLoader 加载勘探震源文件，和生成样例文件

 2、样例文件在：Assets\StreamingAssets\勘探震源文件.json

 ![image](/M~5OY[QBRA`%PH\)%}O74P1D.png)

 ## 更新日期：2023-09-08

 1、新增ExplorationPointsLoader卸载功能

 2、新增QuakePointsLoader卸载功能

 3、新增QuakeLinesLoader 震源车轨迹运动功能

 ![image](/6}}MHC}OGCYP_0FYJ~L9OTP.png)

 ![video](/f82720397b414b7ed951a41ef5e4d98b.mp4)

 ## 更新日期：2023-09-09

 1、新增RoamingController 漫游功能，需要自主编辑帧动画然后使用该控制器进行展示

  ![video](/2023-09-08%2023-47-39.mp4)

 