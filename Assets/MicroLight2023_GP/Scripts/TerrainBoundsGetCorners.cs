using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class TerrainBoundsGetCorners : MonoBehaviour
{
    public Terrain terrain;

    public TerrainCollider terrainCollider;
    /// <summary>
    /// �ϲ㶥��ǣ�2 3 7 6
    /// �²㶥��ǣ�0 1 5 4
    /// </summary>

    public List<Vector3> TerrainColliderCornersWorldCorners = new List<Vector3>();


    private TerrainData terrainData;

    private TerrainCollider TempterrainCollider;

    private Vector3 TempterrainPosition;
    private Vector3 TempterrainRotation;
    private Vector3 TempterrainScale;


    // Start is called before the first frame update
    void Start()
    {
        UpdateTerrainColliderCornersWorldCorners();

       
    }

    // Update is called once per frame
    void Update()
    {
        bool needtoupdate = false;

        if (terrain)
        {
            if (terrain.terrainData)
            {
                if (terrainData != terrain.terrainData)
                {
                    terrainData = terrain.terrainData;
                    needtoupdate = true;
                }

              
            }

            if (TempterrainPosition != terrain.transform.position)
            {
                TempterrainPosition = terrain.transform.position;
                needtoupdate = true;
            }


            if (TempterrainRotation != terrain.transform.eulerAngles)
            {
                TempterrainRotation = terrain.transform.eulerAngles;
                needtoupdate = true;
            }


            if (TempterrainScale != terrain.transform.lossyScale)
            {
                TempterrainScale = terrain.transform.lossyScale;
                needtoupdate = true;
            }
        }

        if (terrainCollider)
        {
            if (TempterrainCollider != terrainCollider)
            {
                TempterrainCollider = terrainCollider;
                needtoupdate = true;
            }
        }

        if(needtoupdate)
        {
            UpdateTerrainColliderCornersWorldCorners();

        }

       
    }

#if UNITY_EDITOR
    public Color color;
 
    public enum PlaneType
    {
        XY,
        XZ,
        ZY
    }

    [SerializeField]
    protected PlaneType planeType;
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
      
        for (int i = 0; i < TerrainColliderCornersWorldCorners.Count; i++)
        {
            Gizmos.color = Random.ColorHSV();

            UnityEditor.Handles.Label(transform.InverseTransformPoint(TerrainColliderCornersWorldCorners[i]),i.ToString());

            Gizmos.DrawSphere(transform.InverseTransformPoint(TerrainColliderCornersWorldCorners[i]), 10);
        }
        DrawPlane();
    }

    private void DrawPlane()
    {
        Quaternion m_rotation;
        switch (planeType)
        {
            case PlaneType.XY:
                m_rotation = Quaternion.LookRotation(Vector3.forward);
                break;
            case PlaneType.XZ:
                m_rotation = Quaternion.LookRotation(Vector3.up);
                break;
            case PlaneType.ZY:
                m_rotation = Quaternion.LookRotation(Vector3.right);
                break;
            default:
                m_rotation = Quaternion.identity;
                break;
        }

        Matrix4x4 trs = Matrix4x4.TRS(terrainCollider.bounds.center, m_rotation, Vector3.one);
        Gizmos.matrix = trs;
        Color32 m_color = color;
      
        Gizmos.color = m_color;
        Gizmos.DrawCube(Vector3.zero, new Vector3(1.0f, 1.0f, 0.0001f) *  terrainCollider.bounds.size.z);
        Gizmos.matrix = Matrix4x4.identity;
        Gizmos.color = color;
    }
#endif

  
    public void UpdateTerrainColliderCornersWorldCorners()
    {
        TerrainColliderCornersWorldCorners = AlgorithmUtils.GetTerrainColliderCornersWorld(terrainCollider);
    }


  
}
