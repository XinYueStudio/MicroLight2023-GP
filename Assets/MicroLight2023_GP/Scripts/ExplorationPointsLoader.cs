using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 勘探打井点文件数据结构
/// </summary>
[System.Serializable]
public class ExplorationPoints
{
    /// <summary>
    /// 勘探打井地块右下角经纬度坐标
    /// </summary>
    public Vector2 BottomRightLatLon;
    /// <summary>
    /// 勘探打井地块左上角经纬度坐标
    /// </summary>
    public Vector2 TopLeftLatLon;
    /// <summary>
    /// 勘探打井地块打井点列表
    /// </summary>
    public List<ExplorationPoint> Points = new List<ExplorationPoint>();
}
/// <summary>
/// 勘探打井点数据结构
/// </summary>
[System.Serializable]
public class ExplorationPoint
{
    /// <summary>
    /// 勘探点编号
    /// </summary>
    public string Id;
    /// <summary>
    /// 勘探点纬度坐标
    /// </summary>
    public Vector2 Point;
    /// <summary>
    /// 勘探点深度
    /// </summary>
    public float Depth;
    /// <summary>
    /// 勘探点标记颜色
    /// </summary>
    public Vector4 RGBA;
}


[System.Serializable]
public class ExplorationPointGameObject
{
    /// <summary>
    /// 勘探转井点数据
    /// </summary>
    public ExplorationPoint PointData;
    /// <summary>
    /// 柱体
    /// </summary>
    public GameObject Cylinder;
}
[RequireComponent(typeof(TerrainBoundsGetCorners))]
public class ExplorationPointsLoader : MonoBehaviour
{

    private TerrainBoundsGetCorners mTerrainCorners;

    public TerrainBoundsGetCorners TerrainCorners
    {
        get
        {
            if(mTerrainCorners==null)
            {
                mTerrainCorners = GetComponent<TerrainBoundsGetCorners>();
            }
            return mTerrainCorners;
        }
    }


    [HideInInspector]
    public ExplorationPoints ExplorationPoints = new ExplorationPoints();
    [HideInInspector]
    public List<ExplorationPointGameObject> ExplorationPointGameObjects = new List<ExplorationPointGameObject>();
    [HideInInspector]
    [Range(1,100)]
    public float ExplorationPointScale = 10f;
    /// <summary>
    /// 加载
    /// </summary>
    /// <param name="data"></param>

    public void Load(ExplorationPoints explorationPoints)
    {
        ExplorationPoints = explorationPoints;

        //清理旧数据
        Clear();

        if (TerrainCorners)
        {
            //更新Unity顶点数据
            TerrainCorners.UpdateTerrainColliderCornersWorldCorners();
            //勘探打井地块右下角经纬度坐标
            Vector2 BottomRightLatLon = ExplorationPoints.BottomRightLatLon;
            //勘探打井地块左上角经纬度坐标
            Vector2 TopLeftLatLon = ExplorationPoints.TopLeftLatLon;
            //Unity右下角World坐标点
            Vector3 BottomRightPoint = TerrainCorners.TerrainColliderCornersWorldCorners[6];
            //Unity左上角World坐标点
            Vector3 TopLeftPoint = TerrainCorners.TerrainColliderCornersWorldCorners[3];

            for (int i = 0; i < explorationPoints.Points.Count; i++)
            {
                
                   ExplorationPointGameObject explorationPointGameObject = new ExplorationPointGameObject();
                explorationPointGameObject.PointData = explorationPoints.Points[i];

                Vector3 unitypoint = AlgorithmUtils.GetWorldPoint(BottomRightLatLon, TopLeftLatLon, explorationPoints.Points[i].Point, BottomRightPoint, TopLeftPoint);
                Vector3 unitypointOn  =   AlgorithmUtils.GetWorldToTerrainPos(unitypoint, TerrainCorners.terrain);
                explorationPointGameObject.Cylinder = AlgorithmUtils.CreateCylinder(this.transform, unitypointOn - Vector3.up * explorationPoints.Points[i].Depth + Vector3.up*5, Vector3.zero, new Vector3(ExplorationPointScale,  explorationPoints.Points[i].Depth+10, ExplorationPointScale),
                    new Color(explorationPoints.Points[i].RGBA.x, explorationPoints.Points[i].RGBA.y, explorationPoints.Points[i].RGBA.z, explorationPoints.Points[i].RGBA.w));
                explorationPointGameObject.Cylinder.name= "  ExplorationPoint_" + explorationPoints.Points[i].Id;
                ExplorationPointGameObjects.Add(explorationPointGameObject);

            }
        }

    }/// <summary>
     /// 卸载
     /// </summary>
    public void Clear()
    {
        if (ExplorationPointGameObjects.Count > 0)
        {
            for (int i = 0; i < ExplorationPointGameObjects.Count; i++)
            {
                if (ExplorationPointGameObjects[i].Cylinder)
                {
                    DestroyImmediate(ExplorationPointGameObjects[i].Cylinder);
                }
            }
        }
        ExplorationPointGameObjects.Clear();
    }
}
