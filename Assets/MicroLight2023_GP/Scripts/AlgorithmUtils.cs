using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlgorithmUtils 
{
    /// <summary>
    /// 计算地形八角顶点坐标
    ///  
    /// 上层顶点角：2 3 7 6
    /// 下层顶点角：0 1 5 4
    /// 
    /// </summary>
    /// <param name="boxCollider">地形碰撞体</param>
    /// <returns></returns>
    public static List<Vector3> GetTerrainColliderCornersWorld(TerrainCollider boxCollider)
    {
        Vector3 size = boxCollider.bounds.size;
        List<Vector3> list = new List<Vector3>();
        var signs = new List<int> { -1, 2 };
        signs.ForEach(signX =>
            signs.ForEach(signY =>

                signs.ForEach(signZ => {
                    if (signY == -1) signY = 0;
                    if (signZ == 2) signZ = 1;
                    if (signX == 2) signX = 1;
                    var vector = new Vector3(size.x * signX, size.y * signY, size.z * signZ);
                    list.Add(boxCollider.transform.TransformPoint(boxCollider.bounds.center + vector * 0.5f));
                })));

        return list;
    }

    /// <summary>
    /// 输入一个经纬度得到Unity 位置点
    /// </summary>
    /// <param name="BottomRightLatLon">右下角经纬度坐标</param>
    /// <param name="TopLeftLatLon">左上角经纬度坐标</param>
    /// <param name="LatLon">输入一个经纬度</param>
    /// <param name="BottomRightPoint">Unity右下角World坐标点</param>
    /// <param name="TopLeftPoint">Unity左上角World坐标点</param>
    /// <returns></returns>
    public static Vector3 GetWorldPoint(Vector2 BottomRightLatLon, Vector2 TopLeftLatLon, Vector2 LatLon, Vector3 BottomRightPoint, Vector3 TopLeftPoint)
    {
        float z_offset, x_offset, z_w_offset, x_w_offset;

        z_offset = BottomRightLatLon.y - TopLeftLatLon.y;//地图中的维度差  
        x_offset = BottomRightLatLon.x - TopLeftLatLon.x;//地图中的经度差  
        z_w_offset = BottomRightPoint.z - TopLeftPoint.z;//unity中的维度差  
        x_w_offset = BottomRightPoint.x - TopLeftPoint.x;//unity中的经度差  

        float tempX = LatLon.x - TopLeftLatLon.x;
        float tempZ = LatLon.y - BottomRightLatLon.y;
        float _tempX = (tempX * x_w_offset / x_offset + TopLeftPoint.x);
        float _tempZ = (tempZ * z_w_offset / z_offset + BottomRightPoint.z);
        //坐标偏差（在Unity中的坐标）
        return new Vector3(_tempX, BottomRightPoint.y, _tempZ);
    }
    /// <summary>
    /// 由位置点得到经纬度  
    /// </summary>
    /// <param name="curPoint"></param>
    /// <returns></returns>
    /// <summary>
    /// 输入一个Unity 位置点 得到经纬度
    /// </summary>
    /// <param name="BottomRightLatLon">右下角经纬度坐标</param>
    /// <param name="TopLeftLatLon">左上角经纬度坐标</param>
    /// <param name="curPoint">输入一个Unity 位置点 </param>
    /// <param name="BottomRightPoint">Unity右下角World坐标点</param>
    /// <param name="TopLeftPoint">Unity左上角World坐标点</param>
    /// <returns></returns>
    public static Vector2 GetLatLon(Vector2 BottomRightLatLon, Vector2 TopLeftLatLon, Vector3 curPoint, Vector3 BottomRightPoint, Vector3 TopLeftPoint)
    {
        float z_offset, x_offset, z_w_offset, x_w_offset;

        z_offset = BottomRightLatLon.y - TopLeftLatLon.y;//地图中的维度差  
        x_offset = BottomRightLatLon.x - TopLeftLatLon.x;//地图中的经度差  
        z_w_offset = BottomRightPoint.z - TopLeftPoint.z;//unity中的维度差  
        x_w_offset = BottomRightPoint.x - TopLeftPoint.x;//unity中的经度差  
        //坐标偏差
        float _x_offset = (curPoint.x - BottomRightPoint.x) * x_offset / x_w_offset;
        float _z_offset = (curPoint.z - TopLeftPoint.z) * z_offset / z_w_offset;
        float resultX = _x_offset + BottomRightLatLon.x;
        float resultZ = _z_offset + TopLeftLatLon.y;
        return new Vector2(resultX, resultZ);
    }

    /// <summary>
    /// 创建柱体
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="position"></param>
    /// <param name="eulerAngles"></param>
    /// <param name="localScale"></param>
    /// <param name="color"></param>
    /// <returns></returns>
    public static GameObject CreateCylinder(Transform parent,Vector3 position,Vector3 eulerAngles, Vector3 localScale,Color color)
    {
        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cylinder.transform.parent = parent;
        cylinder.transform.position = position;
        cylinder.transform.eulerAngles = eulerAngles;
        cylinder.transform.localScale = localScale;

        MeshRenderer meshRenderer= cylinder.GetComponent<MeshRenderer>();

        if(meshRenderer)
        {
            Material material = new Material(Shader.Find("Standard"));
            material.color = color;
            meshRenderer.sharedMaterial = material;
        }
        return cylinder;
    }

    /// <summary>
    /// 创建特效
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="parent"></param>
    /// <param name="position"></param>
    /// <param name="eulerAngles"></param>
    /// <param name="localScale"></param>
    /// <returns></returns>
    public static GameObject CreateParticle(GameObject prefab,Transform parent, Vector3 position, Vector3 eulerAngles, Vector3 localScale)
    {
        GameObject Object = GameObject.Instantiate(prefab);
        Object.transform.parent = parent;
        Object.transform.position = position;
        Object.transform.eulerAngles = eulerAngles;
        Object.transform.localScale = localScale;

         
        return Object;
    }

    /// <summary>
    /// 计算山体投影点
    /// </summary>
    /// <param name="worldpos"></param>
    /// <param name="terrain"></param>
    /// <returns></returns>
    public  static Vector3 GetWorldToTerrainPos(Vector3 worldPos, Terrain terrain)
    {

        // 将世界坐标转换为Terrain本地坐标
        Vector3 terrainLocalPos = terrain.transform.InverseTransformPoint(worldPos);

        // 获取Terrain上的高度
        float height = terrain.terrainData.GetHeight(Mathf.RoundToInt(terrainLocalPos.x), Mathf.RoundToInt(terrainLocalPos.z));

        // 将Terrain本地坐标转换为Terrain上的坐标
        Vector3 terrainPos = new Vector3(worldPos.x, worldPos.y + height, worldPos.z);


        //Vector3 terrainPos = worldPos;

        //// 创建一个射线从空间坐标向下发射
        //Ray ray = new Ray(worldPos + Vector3.up * 10000f, Vector3.down);

        //// 声明一个变量来存储射线击中的信息
        //RaycastHit hit;

        //// 如果射线与Terrain碰撞
        //if (terrain.GetComponent<TerrainCollider>().Raycast(ray, out hit, 1000000))
        //{
        //    // 获取Terrain上的坐标
        //    terrainPos = hit.point;
 
        //}




        return terrainPos;
    }
}
