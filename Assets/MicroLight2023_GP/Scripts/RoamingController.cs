using MicroLight;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamingData
{
    public Vector3 HoloAeraPosition;
    public Vector3 HoloAeraLocalScale;
    public Vector3 HoloAeraLocalEulerAngles;
}

[RequireComponent(typeof(Animation))]
public class RoamingController : MonoBehaviour
{
  
  
    [Header("全息区域自动漫游")]
    public bool AutoPlay= false;
    [Header("全息区域漫游循环")]
    public bool Loop = false;




    Animation _animationTimeline;

    public Animation animationTimeline
    {
        get
        {
            if (_animationTimeline == null)
            {
                _animationTimeline = GetComponent<Animation>();
            }
            return _animationTimeline;
        }
    }
    public string AnimationName;

    public void SetFrame(float frame)
    {
        animationTimeline[AnimationName].speed = 0;
        animationTimeline[AnimationName].time = frame;
        animationTimeline.Play(AnimationName);
    }

    [Range(0.0f, 1.0f)]
    public float Frame = 0;
    float mFrame = 0;
   
    [Range(0.001f, 0.1f)]
    public float FrameStep = 0.001f;

    [Range(0, 100)]
    public float FrameSpeed = 5;


    public bool IsPlaying = false;

    // Start is called before the first frame update
    void Start()
    {
 
     
    }



    public void OnEnable()
    {
        if (AutoPlay)
        {
            IsPlaying = true;
        }
    }

    public void Update()
    {
      

        if(IsPlaying)
        {
            Frame = (Frame + FrameStep * FrameSpeed * Time.deltaTime)%1.0f;
           if( Frame<=0.2f&& Loop==false&& mFrame>0.5f)
            {
                IsPlaying = false;
            }

            if (mFrame != Frame)
            {
                SetFrame(Frame);
                mFrame = Frame;
            }
        }
    }


    public void OnDisable()
    {
        Frame = 0;
        if (mFrame != Frame)
        {
            SetFrame(Frame);
            mFrame = Frame;
        }
    }

}
