using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 震源点文件数据结构
/// </summary>
[System.Serializable]
public class QuakeSimulationPoints
{
    /// <summary>
    /// 震源点地块右下角经纬度坐标
    /// </summary>
    public Vector2 BottomRightLatLon;
    /// <summary>
    /// 震源点地块左上角经纬度坐标
    /// </summary>
    public Vector2 TopLeftLatLon;
    /// <summary>
    /// 震源点地块震源点列表
    /// </summary>
    public List<QuakeSimulationPointLine>  Lines = new List<QuakeSimulationPointLine>();
}

/// <summary>
/// 震源点数据结构
/// </summary>
[System.Serializable]
public class QuakeSimulationPointLine
{
    /// <summary>
    /// 编号
    /// </summary>
    public string LineId;
    /// <summary>
    /// 震源点地块震源点列表
    /// </summary>
    public List<QuakeSimulationPoint> Points = new List<QuakeSimulationPoint>();
}

/// <summary>
/// 震源点数据结构
/// </summary>
[System.Serializable]
public class QuakeSimulationPoint
{
    /// <summary>
    /// 震源点编号
    /// </summary>
    public string Id;
    /// <summary>
    /// 震源点纬度坐标
    /// </summary>
    public Vector2 Point;
}
[System.Serializable]
public class QuakeSimulationPointGameObject
{
    /// <summary>
    /// 震源点数据
    /// </summary>
    public QuakeSimulationPoint PointData;
    /// <summary>
    /// 震源特效
    /// </summary>
    public GameObject Particle;
}

[System.Serializable]
public class QuakeSimulationPointGameObjectLine
{
    /// <summary>
    ///  编号
    /// </summary>
    public string LineId;
    /// <summary>
    /// 震源特效
    /// </summary>
    public List<QuakeSimulationPointGameObject> SimulationPointGameObjects = new List<QuakeSimulationPointGameObject>();
}



[RequireComponent(typeof(TerrainBoundsGetCorners))]
public class QuakeSimulationPointsLoader : MonoBehaviour
{
    private TerrainBoundsGetCorners mTerrainCorners;

    public TerrainBoundsGetCorners TerrainCorners
    {
        get
        {
            if (mTerrainCorners == null)
            {
                mTerrainCorners = GetComponent<TerrainBoundsGetCorners>();
            }
            return mTerrainCorners;
        }
    }
    [HideInInspector]
    public QuakeSimulationPoints QuakeSimulationPoints = new QuakeSimulationPoints();
    [HideInInspector]
    public List<QuakeSimulationPointGameObjectLine> QuakeSimulationPointGameObjectLines = new List<QuakeSimulationPointGameObjectLine>();

    public GameObject ParticlePrefab;

    /// <summary>
    /// 加载
    /// </summary>
    /// <param name="data"></param>
    public void Load(QuakeSimulationPoints quakeSimulationPoints)
    {
        QuakeSimulationPoints = quakeSimulationPoints;

        //清理旧数据
        Clear();

        if (TerrainCorners)
        {
            //更新Unity顶点数据
            TerrainCorners.UpdateTerrainColliderCornersWorldCorners();
            //勘探打井地块右下角经纬度坐标
            Vector2 BottomRightLatLon = QuakeSimulationPoints.BottomRightLatLon;
            //勘探打井地块左上角经纬度坐标
            Vector2 TopLeftLatLon = QuakeSimulationPoints.TopLeftLatLon;
            //Unity右下角World坐标点
            Vector3 BottomRightPoint = TerrainCorners.TerrainColliderCornersWorldCorners[6];
            //Unity左上角World坐标点
            Vector3 TopLeftPoint = TerrainCorners.TerrainColliderCornersWorldCorners[3];

            for (int i = 0; i < quakeSimulationPoints.Lines.Count; i++)
            {
                QuakeSimulationPointGameObjectLine simulationPointGameObjectLine = new QuakeSimulationPointGameObjectLine();
                simulationPointGameObjectLine.LineId = quakeSimulationPoints.Lines[i].LineId;
                for (int j = 0; j < quakeSimulationPoints.Lines[i].Points.Count; j++)
                {

                    QuakeSimulationPointGameObject quakeSimulationPointGameObject = new QuakeSimulationPointGameObject();
                    quakeSimulationPointGameObject.PointData = quakeSimulationPoints.Lines[i].Points[j];

                    Vector3 unitypoint = AlgorithmUtils.GetWorldPoint(BottomRightLatLon, TopLeftLatLon, quakeSimulationPoints.Lines[i].Points[j].Point, BottomRightPoint, TopLeftPoint);

                    quakeSimulationPointGameObject.Particle = AlgorithmUtils.CreateParticle(ParticlePrefab, this.transform, unitypoint, Vector3.zero, Vector3.one);
                    quakeSimulationPointGameObject.Particle.name = " QuakeSimulationPoint_" + quakeSimulationPoints.Lines[i].Points[j].Id;
                    simulationPointGameObjectLine.SimulationPointGameObjects.Add(quakeSimulationPointGameObject);
                }
                QuakeSimulationPointGameObjectLines.Add(simulationPointGameObjectLine);
            }
        }

    }
    /// <summary>
    /// 卸载
    /// </summary>
    public void Clear()
    {
        if (QuakeSimulationPointGameObjectLines.Count > 0)
        {
            for (int i = 0; i < QuakeSimulationPointGameObjectLines.Count; i++)
            {
                if (QuakeSimulationPointGameObjectLines[i].SimulationPointGameObjects.Count>0)
                {
                    for (int j= 0; j< QuakeSimulationPointGameObjectLines[i].SimulationPointGameObjects.Count; j++)
                    {
                        DestroyImmediate(QuakeSimulationPointGameObjectLines[i].SimulationPointGameObjects[j].Particle);
                    }
                }
            }
        }
        QuakeSimulationPointGameObjectLines.Clear();
    }
}
