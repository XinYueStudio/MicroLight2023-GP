using MicroLight;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class QuakeLineCard : MonoBehaviour
{  
    /// 震源车轨迹数据
   /// </summary>
    public QuakeLineGameObject  quakeLineGameObject;
    QuakeLinesLoader linesLoader;

    public QuakeLinesLoader LinesLoader { get => linesLoader; set => linesLoader = value; }

    public bool HoloAeraFllow = false;
    [Header("全息区域大小")]
    [Range(50,3000)]
    public float HoloAeraScale =500;
    [Header("全息区域旋转角度便宜量")]
    [Range(0, 360)]
    public float HoloAeraYAngleOffset = 0;

    private Vector3 HoloAeraPosition;
    private Vector3 HoloAeraLocalScale;
    private Vector3 HoloAeraLocalEulerAngles;
    // Start is called before the first frame update
    void Start()
    {
        HoloAeraPosition= MicroLightManager.Instance.transform.position;
        HoloAeraLocalScale = MicroLightManager.Instance.transform.localScale;
        HoloAeraLocalEulerAngles = MicroLightManager.Instance.transform.localEulerAngles;

    }
    int currentTargetIndex = 1;
    // Update is called once per frame
    void Update()
    {
        if (quakeLineGameObject != null)
        {
            if (quakeLineGameObject.Play)
            {
                if (quakeLineGameObject.UnityLineData != null)
                {
                    if (quakeLineGameObject.UnityLineData.Points != null)
                    {
                        if (quakeLineGameObject.UnityLineData.Points.Count > 0)
                        {
                            //TODO:测试代码未做采样处理
                         


                            // 如果对象还没有到达当前目标点位
                            if (quakeLineGameObject.QuakeLineCardBody.transform.position != quakeLineGameObject.UnityLineData.Points[currentTargetIndex])
                            {
                                // 计算当前目标点位的方向和距离
                                Vector3 direction = (quakeLineGameObject.UnityLineData.Points[currentTargetIndex] - quakeLineGameObject.QuakeLineCardBody.transform.position).normalized;
                                float distance = Vector3.Distance(quakeLineGameObject.QuakeLineCardBody.transform.position, quakeLineGameObject.UnityLineData.Points[currentTargetIndex]);

                                // 根据移动速度和帧率，平滑地移动对象
                                quakeLineGameObject.QuakeLineCardBody.transform.position += direction * quakeLineGameObject.UnityLineData.Speed * Time.deltaTime;

                                // 如果对象接近当前目标点位，将其直接设置为目标点位的位置
                                if (distance < quakeLineGameObject.UnityLineData.Speed * Time.deltaTime)
                                {
                                    quakeLineGameObject.QuakeLineCardBody.transform.position = quakeLineGameObject.UnityLineData.Points[currentTargetIndex];
                                }
                                quakeLineGameObject.QuakeLineCardBody.transform.forward=(direction);
                            }
                            else
                            {
                                // 如果对象到达了当前目标点位，切换到下一个目标点位
                                currentTargetIndex = (currentTargetIndex + 1) % quakeLineGameObject.UnityLineData.Points.Count;
                            }

                        }
                    }
                }


                if(HoloAeraFllow)
                {
                    MicroLightManager.Instance.transform.position = Vector3.Lerp(MicroLightManager.Instance.transform.position, quakeLineGameObject.QuakeLineCardBody.transform.position, Time.deltaTime * 5);
                    MicroLightManager.Instance.transform.localScale = Vector3.Lerp(MicroLightManager.Instance.transform.localScale, Vector3.one* HoloAeraScale, Time.deltaTime * 5);
                    MicroLightManager.Instance.transform.localEulerAngles = HoloAeraLocalEulerAngles + Vector3.up*HoloAeraYAngleOffset;
                }
                else
                {
                     MicroLightManager.Instance.transform.position=Vector3.Lerp(MicroLightManager.Instance.transform.position, HoloAeraPosition,Time.deltaTime*5);
                    MicroLightManager.Instance.transform.localScale= Vector3.Lerp(MicroLightManager.Instance.transform.localScale, HoloAeraLocalScale, Time.deltaTime * 5);
                    MicroLightManager.Instance.transform.localEulerAngles= Vector3.Lerp(MicroLightManager.Instance.transform.localEulerAngles, HoloAeraLocalEulerAngles, Time.deltaTime * 5);
                }

            }
            else
            {
                currentTargetIndex = 1;
                quakeLineGameObject.QuakeLineCardBody.transform.position = quakeLineGameObject.UnityLineData.Points[0];
                quakeLineGameObject.QuakeLineCardBody.transform.LookAt( quakeLineGameObject.UnityLineData.Points[1]);
            }
        }
    }

#if UNITY_EDITOR
   

 
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        if (quakeLineGameObject != null)
        {
            if (quakeLineGameObject.UnityLineData != null)
        {
                if (quakeLineGameObject.UnityLineData.Points != null)
                {
                    if (quakeLineGameObject.UnityLineData.Points.Count > 0)
                    {
                        for (int i = 0; i < quakeLineGameObject.UnityLineData.Points.Count - 1; i++)
                        {
                            Gizmos.color =Color.magenta;

                            //UnityEditor.Handles.Label(transform.InverseTransformPoint(UnityLineData.Points[i]), i.ToString());

                         
                            Gizmos.DrawLine(quakeLineGameObject.UnityLineData.Points[i], quakeLineGameObject.UnityLineData.Points[i + 1]);
                        }
                      
                    }
                }
            }
        }
    }

#endif
}
