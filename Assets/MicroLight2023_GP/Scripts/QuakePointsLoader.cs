using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// 震源点文件数据结构
/// </summary>
[System.Serializable]
public class QuakePoints
{
    /// <summary>
    /// 震源点地块右下角经纬度坐标
    /// </summary>
    public Vector2 BottomRightLatLon;
    /// <summary>
    /// 震源点地块左上角经纬度坐标
    /// </summary>
    public Vector2 TopLeftLatLon;
    /// <summary>
    /// 震源点地块震源点列表
    /// </summary>
    public List<QuakePoint> Points = new List<QuakePoint>();
}
/// <summary>
/// 震源点数据结构
/// </summary>
[System.Serializable]
public class QuakePoint
{
    /// <summary>
    /// 震源点编号
    /// </summary>
    public string Id;
    /// <summary>
    /// 震源点纬度坐标
    /// </summary>
    public Vector2 Point;
}
[System.Serializable]
public class QuakePointGameObject
{
    /// <summary>
    /// 震源点数据
    /// </summary>
    public QuakePoint PointData;
    /// <summary>
    /// 震源特效
    /// </summary>
    public GameObject Particle;
}
[RequireComponent(typeof(TerrainBoundsGetCorners))]
public class QuakePointsLoader : MonoBehaviour
{
    private TerrainBoundsGetCorners mTerrainCorners;

    public TerrainBoundsGetCorners TerrainCorners
    {
        get
        {
            if (mTerrainCorners == null)
            {
                mTerrainCorners = GetComponent<TerrainBoundsGetCorners>();
            }
            return mTerrainCorners;
        }
    }
    [HideInInspector]
    public QuakePoints QuakePoints = new QuakePoints();
    [HideInInspector]
    public List<QuakePointGameObject> QuakePointGameObjects = new List<QuakePointGameObject>();

    public GameObject ParticlePrefab;

    /// <summary>
    /// 加载
    /// </summary>
    /// <param name="data"></param>
    public void Load(QuakePoints quakePoints)
    {
        QuakePoints = quakePoints;

        //清理旧数据
        Clear();

        if (TerrainCorners)
        {
            //更新Unity顶点数据
            TerrainCorners.UpdateTerrainColliderCornersWorldCorners();
            //勘探打井地块右下角经纬度坐标
            Vector2 BottomRightLatLon = QuakePoints.BottomRightLatLon;
            //勘探打井地块左上角经纬度坐标
            Vector2 TopLeftLatLon = QuakePoints.TopLeftLatLon;
            //Unity右下角World坐标点
            Vector3 BottomRightPoint = TerrainCorners.TerrainColliderCornersWorldCorners[6];
            //Unity左上角World坐标点
            Vector3 TopLeftPoint = TerrainCorners.TerrainColliderCornersWorldCorners[3];

            for (int i = 0; i < quakePoints.Points.Count; i++)
            {

                QuakePointGameObject quakePointGameObject = new QuakePointGameObject();
                quakePointGameObject.PointData = quakePoints.Points[i];

                Vector3 unitypoint = AlgorithmUtils.GetWorldPoint(BottomRightLatLon, TopLeftLatLon, quakePoints.Points[i].Point, BottomRightPoint, TopLeftPoint);

                quakePointGameObject.Particle = AlgorithmUtils.CreateParticle(ParticlePrefab, this.transform, unitypoint, Vector3.zero, Vector3.one);
                quakePointGameObject.Particle.name = " QuakePoint_" + quakePoints.Points[i].Id;
                QuakePointGameObjects.Add(quakePointGameObject);

            }
        }

    }
    /// <summary>
    /// 卸载
    /// </summary>
    public void Clear()
    {
        if (QuakePointGameObjects.Count > 0)
        {
            for (int i = 0; i < QuakePointGameObjects.Count; i++)
            {
                if (QuakePointGameObjects[i].Particle)
                {
                    DestroyImmediate(QuakePointGameObjects[i].Particle);
                }
            }
        }
        QuakePointGameObjects.Clear();
    }
}
