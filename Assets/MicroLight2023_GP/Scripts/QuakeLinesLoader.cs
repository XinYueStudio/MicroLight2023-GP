using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 震源车轨迹文件数据结构
/// </summary>
[System.Serializable]
public class QuakeLines
{
    /// <summary>
    ///震源车轨迹地块右下角经纬度坐标
    /// </summary>
    public Vector2 BottomRightLatLon;
    /// <summary>
    ///震源车轨迹地块左上角经纬度坐标
    /// </summary>
    public Vector2 TopLeftLatLon;
    /// <summary>
    /// 震源车轨迹列表
    /// </summary>
    public List<QuakeLine> Lines = new List<QuakeLine>();
}
/// <summary>
/// 震源车轨迹数据结构
/// </summary>
[System.Serializable]
public class QuakeLine
{
    /// <summary>
    /// 震源车轨迹编号
    /// </summary>
    public string Id;
    /// <summary>
    /// 移动速度
    /// </summary>
    public float Speed;
    /// <summary>
    /// 两点之间自动采样率:震源车在两点之间运动采样地面投影点防止地面不平导致穿模
    /// </summary>
    public int Sample;
    /// <summary>
    /// 震源车轨迹点纬度坐标
    /// </summary>
    public List<Vector2> Points = new List<Vector2>();
}

/// <summary>
/// 震源车轨迹数据结构
/// </summary>
[System.Serializable]
public class QuakeLineUnity
{
    /// <summary>
    /// 震源车轨迹编号
    /// </summary>
    public string Id;
    /// <summary>
    /// 移动速度
    /// </summary>
    public float Speed;
    /// <summary>
    /// 两点之间自动采样率:震源车在两点之间运动采样地面投影点防止地面不平导致穿模
    /// </summary>
    public int Sample;
    /// <summary>
    /// 震源车轨迹点Unity坐标
    /// </summary>
    public List<Vector3> Points = new List<Vector3>();
}

[System.Serializable]
public class QuakeLineGameObject
{
    /// <summary>
    /// 震源车轨迹数据
    /// </summary>
    public QuakeLine LineData;
    /// 震源车轨迹数据
    /// </summary>
    public QuakeLineUnity UnityLineData;

    /// <summary>
    /// 震源车
    /// </summary>
    public QuakeLineCard QuakeLineCardBody;

    /// <summary>
    /// 开启播放
    /// </summary>
    public bool Play = false;
}

[RequireComponent(typeof(TerrainBoundsGetCorners))]
public class QuakeLinesLoader : MonoBehaviour
{

    private TerrainBoundsGetCorners mTerrainCorners;

    public TerrainBoundsGetCorners TerrainCorners
    {
        get
        {
            if (mTerrainCorners == null)
            {
                mTerrainCorners = GetComponent<TerrainBoundsGetCorners>();
            }
            return mTerrainCorners;
        }
    }
    [HideInInspector]
    public QuakeLines quakeLines = new QuakeLines();
    [HideInInspector]
    public List<QuakeLineGameObject> QuakeLineGameObjects = new List<QuakeLineGameObject>();
    /// <summary>
    /// 震源车预制体
    /// </summary>
    public QuakeLineCard QuakeLineCardPrefab;


    

    /// <summary>
    /// 加载
    /// </summary>
    /// <param name="data"></param>
    public void Load(QuakeLines data)
    {
        quakeLines = data;

        //清理旧数据
        Clear();

        if (TerrainCorners)
        {
            //更新Unity顶点数据
            TerrainCorners.UpdateTerrainColliderCornersWorldCorners();
            //勘探打井地块右下角经纬度坐标
            Vector2 BottomRightLatLon = quakeLines.BottomRightLatLon;
            //勘探打井地块左上角经纬度坐标
            Vector2 TopLeftLatLon = quakeLines.TopLeftLatLon;
            //Unity右下角World坐标点
            Vector3 BottomRightPoint = TerrainCorners.TerrainColliderCornersWorldCorners[6];
            //Unity左上角World坐标点
            Vector3 TopLeftPoint = TerrainCorners.TerrainColliderCornersWorldCorners[3];

            for (int i = 0; i < quakeLines.Lines.Count; i++)
            {

                QuakeLineGameObject quakeLineGameObject = new QuakeLineGameObject();
                quakeLineGameObject.LineData = quakeLines.Lines[i];
                quakeLineGameObject.UnityLineData = new QuakeLineUnity();
                quakeLineGameObject.UnityLineData.Id = quakeLines.Lines[i].Id;
                quakeLineGameObject.UnityLineData.Speed = quakeLines.Lines[i].Speed;
                quakeLineGameObject.UnityLineData.Sample = quakeLines.Lines[i].Sample;
                //TODO:把所有的点转换成Unity坐标
                for (int j = 0; j < quakeLines.Lines[i].Points.Count; j++)
                {
                    // 要投影的空间坐标
                    Vector3 unitypoint = AlgorithmUtils.GetWorldPoint(BottomRightLatLon, TopLeftLatLon, quakeLines.Lines[i].Points[j], BottomRightPoint, TopLeftPoint);

                    Vector3 Onterrainpoint = AlgorithmUtils.GetWorldToTerrainPos(unitypoint, TerrainCorners.terrain);

                    quakeLineGameObject.UnityLineData.Points.Add(Onterrainpoint);
                }
                //TODO:克隆震源车并赋予轨迹数据
                quakeLineGameObject.QuakeLineCardBody = GameObject.Instantiate<QuakeLineCard>(QuakeLineCardPrefab, quakeLineGameObject.UnityLineData.Points[0],Quaternion.identity, this.transform);
                quakeLineGameObject.QuakeLineCardBody.transform.LookAt(quakeLineGameObject.UnityLineData.Points[1]);
                quakeLineGameObject.QuakeLineCardBody.name = "QuakeLine_" + quakeLines.Lines[i].Id;
                quakeLineGameObject.QuakeLineCardBody.quakeLineGameObject = quakeLineGameObject;
                quakeLineGameObject.QuakeLineCardBody.LinesLoader = this;
             QuakeLineGameObjects.Add(quakeLineGameObject);

            }
        }

    }
    /// <summary>
    /// 卸载
    /// </summary>
    public void Clear()
    {
        //清理旧数据
        if (QuakeLineGameObjects.Count > 0)
        {
            for (int i = 0; i < QuakeLineGameObjects.Count; i++)
            {
                if (QuakeLineGameObjects[i].QuakeLineCardBody)
                {
                    DestroyImmediate(QuakeLineGameObjects[i].QuakeLineCardBody.gameObject);
                }
            }
        }
        QuakeLineGameObjects.Clear();
    }

}
