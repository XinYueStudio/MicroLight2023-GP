using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(ExplorationPointsLoader))]
public class ExplorationPointsLoaderEditor : Editor
{


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        ExplorationPointsLoader loader = target as ExplorationPointsLoader;

     
         float scale   = EditorGUILayout.Slider("ExplorationPointScale", loader.ExplorationPointScale, 1, 100);

        if (loader.ExplorationPointScale != scale)
        {
            loader.ExplorationPointScale = scale;
            if (loader.ExplorationPointGameObjects.Count > 0)
            {
                for (int i = 0; i < loader.ExplorationPointGameObjects.Count; i++)
                {
                    if (loader.ExplorationPointGameObjects[i].Cylinder)
                    {
                        loader.ExplorationPointGameObjects[i].Cylinder.transform.localScale = new Vector3(scale, loader.ExplorationPointGameObjects[i].Cylinder.transform.localScale.y, scale);
                    }
                }
            }
        }



        if (GUILayout.Button("制作勘探转井文件样例"))
        {
            ExplorationPoints explorationPoints = new ExplorationPoints();
            explorationPoints.TopLeftLatLon = new Vector2(39.403654f, 116.558888f);
            Vector2 TopRightLatLon = new Vector2(39.404184f, 116.746132f);
            explorationPoints.BottomRightLatLon = new Vector2(39.308263f, 116.745408f);
            List<ExplorationPoint> Points = new List<ExplorationPoint>();
            float leftrightdistance = Vector2.Distance(explorationPoints.TopLeftLatLon, TopRightLatLon);
            float topbootomdistance = Vector2.Distance(explorationPoints.BottomRightLatLon, TopRightLatLon);

            Vector2 leftrightdirection = (TopRightLatLon - explorationPoints.TopLeftLatLon).normalized;

            Vector2 topbootomdirection = (explorationPoints.BottomRightLatLon - TopRightLatLon).normalized;

            for (int w = 0; w < 10; w++)
            {
                for (int h = 0; h < 10; h++)
                {
                    Vector2 newpoint = explorationPoints.TopLeftLatLon + (leftrightdirection * w) * (leftrightdistance / 10.0f) - (leftrightdirection * 1) * (leftrightdistance / 10.0f) * 0.5f + (topbootomdirection * h) * (topbootomdistance / 10.0f) + (topbootomdirection * 1) * (topbootomdistance / 10.0f) * 0.5f;

                    ExplorationPoint point = new ExplorationPoint();
                    point.Depth = 100;
                    point.Id = w.ToString() + "_" + h.ToString();
                    point.Point = newpoint;
                    point.RGBA = new Vector4(Color.red.r, Color.red.g, Color.red.b, Color.red.a);
                    Points.Add(point);
                }
            }
            explorationPoints.Points = Points;

            string filedata = JsonUtility.ToJson(explorationPoints);
            string path = Application.streamingAssetsPath + "/勘探转井文件.json";

            if (!Directory.Exists(Application.streamingAssetsPath))
                Directory.CreateDirectory(Application.streamingAssetsPath);

            if (File.Exists(path))
            {
                File.Delete(path);


            }
            File.WriteAllText(path, filedata);
        }
        if (GUILayout.Button("载入勘探转井文件"))
        {
            string path = EditorUtility.OpenFilePanel("打开勘探转井文件", "", "json");
            if (path.Length != 0)
            {
                string filedata = File.ReadAllText(path);

                ExplorationPoints explorationPoints = JsonUtility.FromJson<ExplorationPoints>(filedata);
                if (explorationPoints != null)
                {
                    if (explorationPoints.Points != null)
                    {
                        if (explorationPoints.Points.Count > 0)
                        {
                            loader.Load(explorationPoints);
                        }
                    }
                }
            }
        }
        if (GUILayout.Button("卸载勘探转井文件"))
        {
            loader.Clear();
        }
    }
}

#endif
