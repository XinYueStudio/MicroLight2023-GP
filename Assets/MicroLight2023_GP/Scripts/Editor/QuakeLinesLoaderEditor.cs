using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(QuakeLinesLoader))]
public class QuakeLinesLoaderEditor : Editor
{


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        QuakeLinesLoader loader = target as QuakeLinesLoader;

        if (GUILayout.Button("制作震源车轨迹文件样例"))
        {
            QuakeLines quakeLines = new QuakeLines();
            quakeLines.TopLeftLatLon = new Vector2(39.403654f, 116.558888f);
            Vector2 TopRightLatLon = new Vector2(39.404184f, 116.746132f);
            quakeLines.BottomRightLatLon = new Vector2(39.308263f, 116.745408f);
             List<QuakeLine> Lines = new List<QuakeLine>();
            float leftrightdistance = Vector2.Distance(quakeLines.TopLeftLatLon, TopRightLatLon);
            float topbootomdistance = Vector2.Distance(quakeLines.BottomRightLatLon, TopRightLatLon);

            Vector2 leftrightdirection = (TopRightLatLon - quakeLines.TopLeftLatLon).normalized;
           
            Vector2 topbootomdirection = (quakeLines.BottomRightLatLon- TopRightLatLon).normalized;

            QuakeLine line = new QuakeLine();
            line.Id  ="0";
            float len = 10.0f;
            for (int w=0;w< len; w++)
            {
                for (int h = 0; h < len; h++)
                {
                    Vector2 newpoint = quakeLines.TopLeftLatLon + (leftrightdirection * w)* (leftrightdistance/ len) - (leftrightdirection * 1) * (leftrightdistance / len) *0.5f + (topbootomdirection * h) * (topbootomdistance / len) + (topbootomdirection * 1) * (topbootomdistance / len) *0.5f;


                  
                    line.Points.Add( newpoint);
                   
                }
            }

            line.Speed = 500.0f;
            line.Sample = 10;

            Lines.Add(line);
            quakeLines.Lines = Lines;

            string filedata =    JsonUtility.ToJson(quakeLines);
            string path = Application.streamingAssetsPath + "/震源车轨迹文件.json";

            if (!Directory.Exists(Application.streamingAssetsPath))
                Directory.CreateDirectory(Application.streamingAssetsPath);

            if (File.Exists(path))
            {
                File.Delete(path);

               
            }
            File.WriteAllText(path, filedata);
        }
        if (GUILayout.Button("载入震源车轨迹文件"))
        {
            string path = EditorUtility.OpenFilePanel("打开震源车轨迹文件", "", "json");
            if (path.Length != 0)
            {
                string filedata = File.ReadAllText(path);

                QuakeLines quakeLines = JsonUtility.FromJson<QuakeLines>(filedata);
                if (quakeLines != null)
                {
                    if (quakeLines.Lines != null)
                    {
                        if (quakeLines.Lines.Count>0)
                        {
                            loader.Load(quakeLines);
                        }
                    }
                }
            }
        }
        if (GUILayout.Button("卸载震源车轨迹文件"))
        {
            loader.Clear();
        }

        //if (Application.isPlaying)
        //{
        //    if (loader.QuakeLineGameObjects.Count > 0)
        //    {
        //        for (int i = 0; i < loader.QuakeLineGameObjects.Count; i++)
        //        {
        //            loader.QuakeLineGameObjects[i].Play=  GUILayout.Toggle(loader.QuakeLineGameObjects[i].Play, "播放震源车轨迹 " + loader.QuakeLineGameObjects[i].UnityLineData.Id);
        //        }
        //    }
        //}
    }
}

#endif
