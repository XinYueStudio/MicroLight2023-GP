using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(QuakePointsLoader))]
public class QuakePointsLoaderEditor: Editor
{


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        QuakePointsLoader loader = target as QuakePointsLoader;

        if (GUILayout.Button("制作勘探震源文件样例"))
        {
            QuakePoints quakePoints = new QuakePoints();
            quakePoints.TopLeftLatLon = new Vector2(39.403654f, 116.558888f);
            Vector2 TopRightLatLon = new Vector2(39.404184f, 116.746132f);
            quakePoints.BottomRightLatLon = new Vector2(39.308263f, 116.745408f);
             List<QuakePoint> Points = new List<QuakePoint>();
            float leftrightdistance = Vector2.Distance(quakePoints.TopLeftLatLon, TopRightLatLon);
            float topbootomdistance = Vector2.Distance(quakePoints.BottomRightLatLon, TopRightLatLon);

            Vector2 leftrightdirection = (TopRightLatLon - quakePoints.TopLeftLatLon).normalized;
           
            Vector2 topbootomdirection = (quakePoints.BottomRightLatLon- TopRightLatLon).normalized;

            float len = 10.0f;
            for (int w=0;w< len; w++)
            {
                for (int h = 0; h < len; h++)
                {
                    Vector2 newpoint = quakePoints.TopLeftLatLon + (leftrightdirection * w)* (leftrightdistance/ len) - (leftrightdirection * 1) * (leftrightdistance / len) *0.5f + (topbootomdirection * h) * (topbootomdistance / len) + (topbootomdirection * 1) * (topbootomdistance / len) *0.5f;

                    QuakePoint point = new QuakePoint();
                    point.Id = w.ToString()+"_"+ h.ToString();
                    point.Point = newpoint;
                    Points.Add(point);
                }
            }
            quakePoints.Points = Points;

            string filedata =    JsonUtility.ToJson(quakePoints);
            string path = Application.streamingAssetsPath + "/勘探震源文件.json";

            if (!Directory.Exists(Application.streamingAssetsPath))
                Directory.CreateDirectory(Application.streamingAssetsPath);

            if (File.Exists(path))
            {
                File.Delete(path);

               
            }
            File.WriteAllText(path, filedata);
        }
        if (GUILayout.Button("载入勘震源井文件"))
        {
            string path = EditorUtility.OpenFilePanel("打开勘震源井文件", "", "json");
            if (path.Length != 0)
            {
                string filedata = File.ReadAllText(path);

                QuakePoints quakePoints = JsonUtility.FromJson<QuakePoints>(filedata);
                if (quakePoints != null)
                {
                    if (quakePoints.Points != null)
                    {
                        if (quakePoints.Points.Count>0)
                        {
                            loader.Load(quakePoints);
                        }
                    }
                }
            }
        }
        if (GUILayout.Button("卸载勘震源井文件"))
        {
            loader.Clear();
        }
    }
}

#endif
