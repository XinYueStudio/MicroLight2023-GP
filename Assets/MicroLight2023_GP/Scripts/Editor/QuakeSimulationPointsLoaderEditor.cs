using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(QuakeSimulationPointsLoader))]
public class QuakeSimulationPointsLoaderEditor : Editor
{


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        QuakeSimulationPointsLoader loader = target as QuakeSimulationPointsLoader;

        if (GUILayout.Button("制作勘探转井爆破仿真文件样例"))
        {
            QuakeSimulationPoints quakeSimulationPoints = new QuakeSimulationPoints();
            quakeSimulationPoints.TopLeftLatLon = new Vector2(39.403654f, 116.558888f);
            Vector2 TopRightLatLon = new Vector2(39.404184f, 116.746132f);
            quakeSimulationPoints.BottomRightLatLon = new Vector2(39.308263f, 116.745408f);
             List<QuakePoint> Points = new List<QuakePoint>();
            float leftrightdistance = Vector2.Distance(quakeSimulationPoints.TopLeftLatLon, TopRightLatLon);
            float topbootomdistance = Vector2.Distance(quakeSimulationPoints.BottomRightLatLon, TopRightLatLon);

            Vector2 leftrightdirection = (TopRightLatLon - quakeSimulationPoints.TopLeftLatLon).normalized;
           
            Vector2 topbootomdirection = (quakeSimulationPoints.BottomRightLatLon- TopRightLatLon).normalized;

            float len = 10.0f;
            for (int w=0;w< len; w++)
            {
                QuakeSimulationPointLine quakeSimulationPointLine = new QuakeSimulationPointLine();
                quakeSimulationPointLine.LineId = w.ToString();
                for (int h = 0; h < len; h++)
                {
                    Vector2 newpoint = quakeSimulationPoints.TopLeftLatLon + (leftrightdirection * w)* (leftrightdistance/ len) - (leftrightdirection * 1) * (leftrightdistance / len) *0.5f + (topbootomdirection * h) * (topbootomdistance / len) + (topbootomdirection * 1) * (topbootomdistance / len) *0.5f;

                    QuakeSimulationPoint point = new QuakeSimulationPoint();
                    point.Id = w.ToString()+"_"+ h.ToString();
                    point.Point = newpoint;
                    quakeSimulationPointLine.Points.Add(point);
                }
                quakeSimulationPoints.Lines.Add(quakeSimulationPointLine);
            }
        

            string filedata =    JsonUtility.ToJson(quakeSimulationPoints);
            string path = Application.streamingAssetsPath + "/勘探转井爆破仿真文件.json";

            if (!Directory.Exists(Application.streamingAssetsPath))
                Directory.CreateDirectory(Application.streamingAssetsPath);

            if (File.Exists(path))
            {
                File.Delete(path);

               
            }
            File.WriteAllText(path, filedata);
        }
        if (GUILayout.Button("载入勘探转井爆破仿真文件"))
        {
            string path = EditorUtility.OpenFilePanel("打开勘探转井爆破仿真文件", "", "json");
            if (path.Length != 0)
            {
                string filedata = File.ReadAllText(path);

                QuakeSimulationPoints quakeSimulationPoints = JsonUtility.FromJson<QuakeSimulationPoints>(filedata);
                if (quakeSimulationPoints != null)
                {
                    if (quakeSimulationPoints.Lines != null)
                    {
                        if (quakeSimulationPoints.Lines.Count>0)
                        {
                            loader.Load(quakeSimulationPoints);
                        }
                    }
                }
            }
        }
        if (GUILayout.Button("勘探转井爆破仿真文件"))
        {
            loader.Clear();
        }
    }
}

#endif
