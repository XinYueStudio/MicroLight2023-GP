Shader "Custom/Hologram" {
	Properties{
		_MainTex("Main Texture", 2D) = "white" {}
		_HologramTex("Hologram Texture", 2D) = "white" {}
		_Opacity("Opacity", Range(0, 1)) = 0.5
		_Color("Color", Color) = (1, 1, 1, 1)
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf Lambert vertex:vert

			sampler2D _MainTex;
			sampler2D _HologramTex;
			half _Opacity;
			fixed4 _Color;

			struct Input {
				float2 uv_MainTex;
				float2 uv_HologramTex;
				float4 screenPos;
			};

			void vert(inout appdata_full v, out Input o) {
				UNITY_INITIALIZE_OUTPUT(Input, o);
				o.uv_MainTex = v.texcoord;
				o.uv_HologramTex = v.texcoord;
				o.screenPos = ComputeScreenPos(v.vertex);
			}

			void surf(Input IN, inout SurfaceOutput o) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				fixed4 hologram = tex2Dproj(_HologramTex, UNITY_PROJ_COORD(IN.screenPos));

				// 获取深度值
				float depth = LinearEyeDepth(IN.screenPos.z);

				// 根据深度值和透明度调整颜色
				c.rgb = lerp(c.rgb, hologram.rgb, depth * _Opacity);

				// 调整颜色
				c.rgb *= _Color.rgb;

				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
			ENDCG
		}
			FallBack "Diffuse"
}
