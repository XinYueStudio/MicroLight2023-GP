﻿/*******************************************************
 * Copyright (C) 2016 Ngan Do - dttngan91@gmail.com
 *******************************************************/
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LeapMotionSimpleControl
{
    public enum GameState {
        None,
        Parts,
        Boom,
        Slice,
    }
	public class GameManager : MonoBehaviour
	{
        public static event Action resetPinches;
        public GestureManager gestureManager;
        public UIManager uiManager;
        public MenuManager menuManager;

        public delegate void EndEvent(GestureManager.GestureTypes type);

        public enum GameMode
        {
            Desktop,
            VR
        }
        public GameState currentState = GameState.None;

        public GameMode CurrentMode;
        public Transform VRCamera;
        public Transform DesktopCamera;


        // Use this for initialization
        void Awake()
        {
            init();
        }

        public GameObject mainObj;
        public Animator boomAni;
        

        public GameObject detector;//关闭了这个就无法移动旋转缩放
        public GameObject grabSlice;//关了这个就不发切割
        public UnityEngine.UI.Text txt;

        public bool nowBoom;

        private int currentPartID = 0;

        public int CurrentPartID
        {
            get => currentPartID;
            set
            {

               
                currentPartID = value;
            }

        }

        public void SelectParts(int id)
        {
            
            
        }

        public void ResetModel()
        {
            
        }


        public void Boom(bool forward)
        {

            boomAni.SetFloat("speed", forward ? 1 : -1);
            float normalizedTime = 0;
            var nowState = boomAni.GetCurrentAnimatorStateInfo(0);
            if (nowState.IsName("default"))
            {
                normalizedTime = 0;
            }
            else if (nowState.IsName("pieces"))
            {
                normalizedTime = nowState.normalizedTime;
                if (normalizedTime <= 0)
                {
                    normalizedTime = 0;

                }
                else if (normalizedTime >= 1)
                {
                    normalizedTime = 1;
                }
                //Mathf.Clamp(normalizedTime,0,1);
            }
            boomAni.Play("pieces", 0, normalizedTime);


            //boomAni.SetFloat("speed", nowBoom ? -1 : 1);
            //txt.text = nowBoom ? "拆解" : "复原";
            //var nowState = boomAni.GetCurrentAnimatorStateInfo(0);
            //float normalizedTime = 0;
            //if (nowState.IsName("default"))
            //{
            //    normalizedTime = 0;
            //}
            //else if (nowState.IsName("pieces"))
            //{
            //    normalizedTime = nowState.normalizedTime;
            //    if (normalizedTime <= 0)
            //    {
            //        normalizedTime = 0;

            //    }
            //    else if (normalizedTime >= 1)
            //    {
            //        normalizedTime = 1;
            //    }
            //    //Mathf.Clamp(normalizedTime,0,1);
            //}

            //boomAni.Play("pieces", 0, normalizedTime);
            //nowBoom = !nowBoom;
        }

        public void ResetBoom()
        {


        }

        private void Update()
        {
           

            if (Input.GetKeyDown(KeyCode.F1))
            {
                //进入动画
                currentState = GameState.Boom;
                Boom(true);
                detector.SetActive(true);
                grabSlice.SetActive(false);
            }

            if (Input.GetKeyDown(KeyCode.F2))
            {
                //进入分解
                currentState = GameState.Parts;
                SelectParts(0);
                detector.SetActive(true);
                grabSlice.SetActive(false);
            }

            if (Input.GetKeyDown(KeyCode.B))
            {
                ResetModel();
            }

            if (Input.GetKeyDown(KeyCode.F3))
            {
                //进入切割
                currentState = GameState.Slice;
                detector.SetActive(false);
                grabSlice.SetActive(true);
            }
        }


        
	
		

		void init ()
		{
            if (DesktopCamera == null)
            {
                CurrentMode = GameMode.VR;
                Debug.Log("Current scene doesn't have Desktop mode");
            }
            else if (VRCamera == null)
            {
                CurrentMode = GameMode.Desktop;
                Debug.Log("Current scene doesn't have VR mode");
            }

            if (DesktopCamera != null)
                DesktopCamera.gameObject.SetActive(CurrentMode == GameMode.Desktop);
            if (VRCamera != null)
                VRCamera.gameObject.SetActive (CurrentMode == GameMode.VR);

			gestureManager.InitGesture (this);
			uiManager.InitUI (this);
		}

		#region Gesture

		public Dictionary<GestureManager.GestureTypes, object> GetCurrentActiveGestures ()
		{
			return gestureManager.GetCurrentActiveGestures ();
		}

		public Transform GetTransformGestureManagerBasedMode ()
		{
			return (CurrentMode == GameMode.Desktop) ? DesktopCamera : VRCamera;
		}

		#endregion

		#region UI

		public bool IsReadyUI ()
		{
			return uiManager.IsReady ();
		}

		public void UpdateUIBlockingGesture (GestureManager.GestureTypes type, float timer, EndEvent endEvent)
		{
			uiManager.RegisterEventEndCountDown (endEvent);
			uiManager.UpdateSliderBlockingGesture (type, timer);
		}

		public void UpdateUILoadingGesture (GestureManager.GestureTypes type, float percent)
		{
			uiManager.UpdateTimerLoadingGesture (type, percent);
		}

		#endregion

		#region Menu

		public void NavigateMenu (GestureManager.GestureTypes type)
		{
			if (menuManager != null) {
				if (type == GestureManager.GestureTypes.SwipingLeft) {
					menuManager.SwipeLeft ();
				} else if (type == GestureManager.GestureTypes.SwipingRight) {
					menuManager.SwipeRight ();
				} 
			}
		}

        public void ChoosePart(GestureManager.GestureTypes type) {
            if (currentState != GameState.Parts)
                return;
            
            switch (type)
            {
                case GestureManager.GestureTypes.SwipingLeft:
                    CurrentPartID += 1;
                    SelectParts(CurrentPartID);
                    break;
                case GestureManager.GestureTypes.SwipingRight:
                    CurrentPartID -= 1;
                    SelectParts(CurrentPartID);
                    break;
                
            }
        }

        public void ShowBoomAni(GestureManager.GestureTypes type) {
            if (currentState != GameState.Boom)
                return;

            switch (type)
            {
                
                case GestureManager.GestureTypes.SwipingUp:
                    Boom(true);
                    break;
                case GestureManager.GestureTypes.SwipingDown:
                    Boom(false);
                    break;
               
            }
        }

		#endregion
	}
}