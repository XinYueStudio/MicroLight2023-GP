﻿using Leap;
using Leap.Unity;
using LeapMotionSimpleControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchBase : MonoBehaviour
{
    private Vector3 startPos;
    private Vector3 startRot;
    private Vector3 startScale;

    Transform player;

    LeapProvider provider;
    List<Hand> _listHands;

    protected float handForwardDegree = 30;

    protected float smallestVelocity = 0.4f;

    [SerializeField]
    private bool canAction;

    public bool CanAction { get => canAction; set => canAction = value; }
    private void Awake()
    {
        GameManager.resetPinches += ResetTrans;
    }

    

    protected virtual void Start() {
        startPos = transform.localPosition;
        startRot = transform.localEulerAngles;
        startScale = transform.localScale;

        provider = FindObjectOfType<LeapProvider>() as LeapProvider;
    }

    public void ResetTrans() {
        transform.localPosition = startPos;
        transform.localEulerAngles = startRot;
        transform.localScale = startScale;
    }


    protected Hand GetCurrent1Hand()
    {
        if (_listHands.Count == 1)
            return _listHands[0];
        else
            return null;
    }
    protected bool isGrabHand(Hand hand)
    {
        return hand.GrabStrength > 0.8f;
    }

    protected bool isOppositeDirection(Vector a, Vector b)
    {
        return angle2LeapVectors(a, b) > (180 - handForwardDegree);
    }

    protected bool isSameDirection(Vector a, Vector b)
    {
        //Debug.Log (angle2LeapVectors (a, b) + " " + b);
        return angle2LeapVectors(a, b) < handForwardDegree;
    }

    protected bool isPalmNormalSameDirectionWith(Hand hand, Vector3 dir)
    {
        return isSameDirection(hand.PalmNormal, UnityVectorExtension.ToVector(dir));
    }

    protected bool isHandMoveForward(Hand hand)
    {
        return isSameDirection(hand.PalmNormal, hand.PalmVelocity) && !isStationary(hand);
    }

    protected float angle2LeapVectors(Leap.Vector a, Leap.Vector b)
    {
        return Vector3.Angle(UnityVectorExtension.ToVector3(a), UnityVectorExtension.ToVector3(b));
    }
    protected bool isStationary(Hand hand)
    {
        return hand.PalmVelocity.Magnitude < smallestVelocity;
    }
    public virtual void Action() {

    }

    public Vector3 getHandVelocity(Hand hand)
    {
        if (player == null)
            player = transform.root;
        return player.InverseTransformDirection(UnityVectorExtension.ToVector3(hand.PalmVelocity));
    }

    protected List<Hand> GetCurrent2Hands()
    {
        if (_listHands.Count == 2)
            return _listHands;
        else
            return null;
    }

    protected virtual void FixedUpdate() {
        //update手臂
        Frame frame = provider.CurrentFrame;
        _listHands = frame.Hands;

        Action();
    }
}
