﻿using Leap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchTranslate : PinchBase
{
    public float sensitive = 5f; //位移灵敏度
    public bool inverse;
    public override void Action()
    {
        if (!CanAction)
            return;

        Hand hand = GetCurrent1Hand();
        if (hand == null)
            return;

        var vector = hand.PalmVelocity;
        Vector3 value = Vector3.zero;
        //value = new Vector3(0, vector.x * rotate_sensitive_Y, 0) * (inverse ? -1 : 1);
        value = new Vector3(vector.x, vector.y, 0) * (inverse ? -1 : 1) * sensitive;
        // transform.localEulerAngles += value;
        transform.localPosition += value;

        //transform.Rotate(transform.localEulerAngles + new Vector3(a.y * rotate_sensitive_X, a.x * rotate_sensitive_Y, 0));
        //Vector3 value = transform.localEulerAngles;
        //value = new Vector3(hand.PalmPosition.y * rotate_sensitive + rotate_initial_value, hand.PalmPosition.x * rotate_sensitive + rotate_initial_value, 0) * (inverse ? -1 : 1);
        //transform.localEulerAngles = value;


    }
}
