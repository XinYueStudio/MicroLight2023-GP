﻿/*******************************************************
 * Copyright (C) 2016 Ngan Do - dttngan91@gmail.com
 *******************************************************/
using UnityEngine;
using System.Collections;
using Leap;

namespace LeapMotionSimpleControl
{
	public class SwipingRightHand : BehaviorHand
	{
        public bool inverse;
		// Use this for initialization
		void Start ()
		{
			_currentType = GestureManager.GestureTypes.SwipingRight;
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		protected override bool checkConditionGesture ()
		{
			Hand hand = GetCurrent1Hand ();
			if (hand != null) {
				if (isOpenFullHand (hand) && isMoveRight (hand)) {
                    if (!inverse)
                    {
                        return true;
                    }
					
				}else 
                if (isOpenFullHand(hand) && isMoveLeft(hand))
                {
                    if (inverse)
                    {
                        return true;
                    }

                }
            }
			return false;
		}
	}
}