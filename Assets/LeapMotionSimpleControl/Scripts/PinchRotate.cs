﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AxisConfig {
    public bool inverse;
    public bool locked;
    public float sensitive = 5f;
}
public class PinchRotate : PinchBase
{
    public AxisConfig axisConfig_X;
    public AxisConfig axisConfig_Y;


    //public float rotate_sensitive_X = 5;  //旋转灵敏度X
    //public float rotate_sensitive_Y = 5;  //旋转灵敏度Y
    
    
    //public bool inverse_x;
    //public bool inverse_y;

    //public bool lockX;
    //public bool lockY;
    public override void Action()
    {
       
        if (!CanAction)
            return;
        Hand hand = GetCurrent1Hand();
        if (hand == null)
            return;

       var vector = hand.PalmVelocity;
        Vector3 value_x = new Vector3(0, vector.x * axisConfig_Y.sensitive, 0) * (axisConfig_Y.inverse ? -1 : 1);
        Vector3 value_y = new Vector3(vector.y * axisConfig_X.sensitive, 0, 0) * (axisConfig_X.inverse ? -1 : 1);
        if (!axisConfig_X.locked)
        {
            //  transform.localEulerAngles += value_x;
            transform.rotation = Quaternion.AngleAxis(value_x.y, Vector3.up) * transform.rotation;
        }
        if (!axisConfig_Y.locked)
        {
            transform.rotation = Quaternion.AngleAxis(value_y.x, Vector3.right) * transform.rotation;

        }


        //transform.Rotate(transform.localEulerAngles + new Vector3(a.y * rotate_sensitive_X, a.x * rotate_sensitive_Y, 0));
        //Vector3 value = transform.localEulerAngles;
        //value = new Vector3(hand.PalmPosition.y * rotate_sensitive + rotate_initial_value, hand.PalmPosition.x * rotate_sensitive + rotate_initial_value, 0) * (inverse ? -1 : 1);
        //transform.localEulerAngles = value;


    }
}
