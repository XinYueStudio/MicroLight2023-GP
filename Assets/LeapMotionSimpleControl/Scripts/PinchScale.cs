﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PinchScale : PinchBase
{
    [Range(0.01f,0.1f)]
    public float sensitive = 0.1f;
    [Range(0.3f,0.8f)]
    public float detectorSenditive = 0.5f;
    public bool inverse;
    public float maxSize = 3f;
    public float minSize = 0.5f;
    List<Hand> currentList;
    Hand leftHand;
    Hand rightHand;
    

    private bool leftReady;
    private bool rightReady;
    
    public bool LeftReady { get => leftReady; set {
            leftReady = value;
            Begin();
        } }
    public bool RightReady { get => rightReady; set {
            rightReady = value;
            Begin();
        } }

    public void Begin() {
        CanAction = LeftReady && RightReady;

        
       
    }
    
    public override void Action()
    {
        //更新手
        currentList = GetCurrent2Hands();
        if (currentList != null)
        {
            leftHand = currentList[0].IsLeft ? currentList[0] : currentList[1];
            rightHand = currentList[0].IsRight ? currentList[0] : currentList[1];
        }

        if (leftHand == null || rightHand == null)
        {
            return;
        }
        //两只手都ready，通过hand上绑定的Pinch Detector脚本判定

        if (!CanAction)
            return;

        //现在肯定是两只手都出现了，判断距离  （看看是否有必要）

        //获取手势达成时的初始位置计算初始距离，后面根据距离来判断缩放程度
        //float offseDis = Vector3.Distance(leftHand.PalmPosition.ToVector3(), rightHand.PalmPosition.ToVector3()) - startDistance;
        //if (offseDis >= 0.1f)
        //{
        //    transform.localScale += Vector3.one * offseDis * sensitive;
        //}

        //if (isOppositeDirection(leftHand.PalmNormal, rightHand.PalmNormal)
        //    && isOppositeDirection(leftHand.PalmVelocity, rightHand.PalmVelocity)
        //    )
        //{
        //        Debug.Log("???");
        //    if (isHandMoveForward(leftHand) && isHandMoveForward(rightHand))
        //    {
        //        transform.localScale += Vector3.one  * sensitive;
        //    }
        //    else if (isHandMoveForward(rightHand) && isHandMoveForward(leftHand))
        //    {
        //      //  transform.localScale += Vector3.one * sensitive;
        //    }

        //}
        var vectorL = leftHand.PalmVelocity;
        var vectorR = rightHand.PalmVelocity;
       
        float temp = (vectorL.x - vectorR.x) * (inverse ? -1 : 1);
        if (Mathf.Abs(temp) >= detectorSenditive)
        {
            transform.localScale += new Vector3(temp, temp, temp) * sensitive;
            if (transform.localScale.x >= maxSize )
            {
                transform.localScale = Vector3.one * maxSize;
            }
            else if (transform.localScale.x <= minSize)
            {
                transform.localScale = Vector3.one * minSize;
            }
        }
       
    }
}
