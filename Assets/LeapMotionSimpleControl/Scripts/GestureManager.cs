﻿/*******************************************************
 * Copyright (C) 2016 Ngan Do - dttngan91@gmail.com
 *******************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Leap.Unity;
using UnityEngine.UI;
using CustomUtils;

namespace LeapMotionSimpleControl
{
	public class GestureManager : MonoBehaviour
	{
	

		public enum GestureTypes
		{
			SwipingLeft,
			SwipingRight,
			SwipingUp,
			SwipingDown,
			ThumbUp,
			ThumbDown,
			Fist,
            OpenHand,
			FaceUp,
			FaceDown,
			ClapHand,
			Grab,
			Throw
		}

		GestureTypes _currentType;

		public GestureTypes GteCurrentGestureType ()
		{
			return _currentType;
		}

		public LeapProvider _leapHandProvider;

		public LeapProvider GetLeapHand ()
		{
			return _leapHandProvider;
		}

		public float TimeBetween2Gestures;

		Dictionary<GestureTypes, object> _listActiveGestures;

		public Dictionary<GestureTypes, object> GetCurrentActiveGestures ()
		{
			return _listActiveGestures;
		}

		GameManager _gameManager;
		public Transform player;

		// Use this for initialization
		void Start ()
		{
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void InitGesture (GameManager manager)
		{
			_gameManager = manager;

			//_leapHandProvider = _gameManager.GetTransformGestureManagerBasedMode ().GetComponentInChildren<LeapProvider> ();
			
			_listActiveGestures = new Dictionary<GestureTypes, object> ();
			foreach (Transform t in transform) {
				if (t.GetComponent<BehaviorHand> () != null) {
					t.GetComponent<BehaviorHand>().SetPlayerTransform(player);
					foreach (GestureTypes type in Enum.GetValues(typeof(GestureTypes))) {
						if (t.name.Equals (type.ToString ()))
							_listActiveGestures.Add (type, t.GetComponent<BehaviorHand> () as object);
					}
					t.GetComponent<BehaviorHand> ().Init (this);
				}
			}

		}

		public bool ReceiveEvent (GestureTypes type)
		{
			if (_gameManager.IsReadyUI ()) {
				_currentType = type;
				_gameManager.UpdateUIBlockingGesture (type, TimeBetween2Gestures, UnBlockGesture);
				_gameManager.NavigateMenu (type);

                //TODO
                _gameManager.ChoosePart(type);
                _gameManager.ShowBoomAni(type);


                return true;
			} 
			return false;
		}

		void UnBlockGesture (GestureTypes type)
		{
			BehaviorHand behavior = (BehaviorHand)_listActiveGestures [type];
			behavior.UnBlockGesture ();
		}

		public void LoadingGestureProgress (GestureTypes type, float percent)
		{
			_gameManager.UpdateUILoadingGesture (type, percent);
		}

		public GameManager.GameMode GetCurrentGameMode ()
		{
			return _gameManager.CurrentMode;
		}
	}
}