﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabSlice : PinchBase
{
    public Vector3 centerPos;
    public bool inverse;
    public float sensitive = 1;
    public Transform player;

    private bool initPos;
    public override void Action()
    {
        Hand hand = GetCurrent1Hand();
        if (hand == null)
            return;

       var pos = hand.PalmPosition.ToVector3();
        var vector = hand.PalmVelocity;
        if (isGrabHand(hand))
        {
            if (!initPos)
            {
                transform.localPosition = new Vector3(-10, 12.6f, -2.6f);
                initPos = true;
            }
            
            Vector3 value = new Vector3(vector.x, 0, 0) * (inverse ? -1 : 1) * sensitive;
            transform.localPosition += value;
        }
        else
        {
            transform.localPosition = new Vector3(-1000,12.6f,-2.6f);
            initPos = false;
        }
        
    }

    
}
