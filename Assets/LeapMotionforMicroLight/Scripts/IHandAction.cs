﻿using Leap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

public enum  mDirection
{
    Unknow,
    Left,
    Right,
    Up,
    Down,
    Forwad,
    Brack
}


public interface IHandAction  {


    bool IsActive();

    /// <summary>
    /// 手是不是静止状态
    /// </summary>
    /// <returns></returns>
    bool IsStationary();


    bool IsMoveLeft();


    bool IsMoveRight();


    bool IsMoveUp();


    bool IsMoveDown();

    bool IsMoveForwad();


    bool IsMoveBrack();

    bool IsFacingCameraForward();

    bool IsFacingCameraBrack();

    bool IsFacingCameraRight();

    bool IsFacingCameraLeft();

    bool IsFacingCameraUp();

    bool IsFacingCameraDown();
    


    mDirection Stroke();

 
    mDirection PalmFace();


    Vector3 PalmFacedirection();


    bool Grabed();


    bool Pinched();


    bool Pinched(Finger.FingerType fingerType);


    bool Good();


    bool Yes();


    bool Ok();
    




}
