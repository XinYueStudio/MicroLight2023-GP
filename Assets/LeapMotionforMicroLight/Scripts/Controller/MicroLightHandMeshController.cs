﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

namespace MicroLight.V3
{
    public class MicroLightHandMeshController : HandModelManager
    {

        public KeyCode mKeyCode = KeyCode.F1;
        public SkinnedMeshRenderer LeftHandSkinnedMesh;
        public SkinnedMeshRenderer RightHandSkinnedMesh;

        public MicroLightLeapServiceProvider mLeapServiceProvider;


        public bool ShowMesh = false;


        private void Start()
        {
            if (mLeapServiceProvider)
            {
                //mLeapServiceProvider.SetPDI(MicroLightPlugin.Vfvector3(MicroLightPlugin.GetDisPlayScale()));
            }
            if (ShowMesh)
            {
                if (LeftHandSkinnedMesh)
                {
                    LeftHandSkinnedMesh.gameObject.SetActive(true);
                }
                if (RightHandSkinnedMesh)
                {
                    RightHandSkinnedMesh.gameObject.SetActive(true);
                }

            }
            else
            {
                if (LeftHandSkinnedMesh)
                {
                    LeftHandSkinnedMesh.gameObject.SetActive(false);
                }
                if (RightHandSkinnedMesh)
                {
                    RightHandSkinnedMesh.gameObject.SetActive(false);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (Input.GetKeyDown(mKeyCode))
            {
                if (ShowMesh)
                {
                    ShowMesh = false;
                    if (LeftHandSkinnedMesh)
                    {
                        LeftHandSkinnedMesh.gameObject.SetActive(false);
                    }
                    if (RightHandSkinnedMesh)
                    {
                        RightHandSkinnedMesh.gameObject.SetActive(false);
                    }
                }
                else
                {
                    ShowMesh = true;
                    if (LeftHandSkinnedMesh)
                    {
                        LeftHandSkinnedMesh.gameObject.SetActive(true);
                    }
                    if (RightHandSkinnedMesh)
                    {
                        RightHandSkinnedMesh.gameObject.SetActive(true);
                    }
                }
            }
        }
    }
}