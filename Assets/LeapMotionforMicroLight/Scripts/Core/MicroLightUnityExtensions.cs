﻿using Leap;
using Leap.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace  MicroLight
{
    public static class UnityMatrixExtension
    {
        /** Conversion factor for millimeters to meters. */
        public static readonly float MM_TO_M = 1e-3f;
        public static LeapTransform GetLeapMatrix(this Transform t, float pdix = 1.0f, float pdiy = 1.0f, float pdiz = 1.0f)
        {
            Vector scale = new Vector(t.lossyScale.x * MM_TO_M * pdix, t.lossyScale.y * MM_TO_M * pdiy, t.lossyScale.z * MM_TO_M * pdiz);
           
            LeapTransform transform = new LeapTransform(t.position.ToVector(), t.rotation.ToLeapQuaternion(), scale);
            transform.MirrorZ(); // Unity is left handed.
            return transform;
        }
    }

}
