﻿/************************************************************************************
Copyright      :   Copyright 2018 MicroLight, LLC. All Rights reserved.
Description    :   MicroLightLeapServiceProvider.cs                  
ProductionDate :  2018-11-16 17:55:08
Author         :   T-CODE
************************************************************************************/

using UnityEngine;
using System.Collections;
using Leap.Unity;
using Leap;

namespace MicroLight.V3
{
  

    public class MicroLightLeapServiceProvider : LeapServiceProvider
    {

        public bool UsePDI = true;
        [HideInInspector]
        public float PDIX = 1.0f;
        [HideInInspector]
        public float PDIY = 1.0f;
        [HideInInspector]
        public float PDIZ = 1.0f;
        protected override void transformFrame(Frame source, Frame dest)
        {
            if (!UsePDI)
            {
                dest.CopyFrom(source).Transform(transform.GetLeapMatrix());

            }
            else
            {
                dest.CopyFrom(source).Transform(transform.GetLeapMatrix(PDIX, PDIY, PDIZ));
            }

        }


        public void SetPDI(Vector3 pdi)
        {
            PDIX = pdi.x;
            PDIY = pdi.y;
            PDIZ = pdi.z;
        }
    }
}
