﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHandActionManager  {


    bool  GrabedAll();

    bool SingleGrabed(Leap.Unity.Chirality chirality);

    bool LeftHand_SingleGrabed();

    bool RightHand_SingleGrabed();

    bool PinchAll();

    bool SinglePinch(Leap.Unity.Chirality chirality);

    bool LeftHand_SinglePinch();

    bool RightHand_SinglePinch();

    bool LeftHand_Single_MoveForwad_FaceCamera();

    bool RightHand_Single_MoveForwad_FaceCamera();

    bool FaceUpAll();

    bool FaceDownAll();

    bool FaceForwadAll();

    bool FaceBrackAll();

    bool LeftHandFaceRight_RightFaceLeft();

    bool LeftHandFaceRightAndMoveRightSingle();

    bool RightHandFaceLeftAndMoveLeftSingle();

}
