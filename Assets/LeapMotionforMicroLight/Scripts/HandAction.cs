﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HandAction : MonoBehaviour, IHandAction
{
    #region Logic variables
    public float handForwardDegree
    {
        get
        {
            return HandActionManager.Instance.handForwardDegree;
        }
    }

    public float gradStrength
    {
        get
        {
            return HandActionManager.Instance.gradStrength;
        }
    }

    public float smallestVelocity
    {
        get
        {
            return HandActionManager.Instance.smallestVelocity;
        }
    }

    public float deltaVelocity
    {
        get
        {
            return HandActionManager.Instance.deltaVelocity;
        }
    }

    public float angleChangeRot
    {
        get
        {
            return HandActionManager.Instance.angleChangeRot;
        }
    }

    public float diffAngle2Hands
    {
        get
        {
            return HandActionManager.Instance.diffAngle2Hands;
        }
    }

    public float diffAngle2Velocity
    {
        get
        {
            return HandActionManager.Instance.diffAngle2Velocity;
        }
    }

    public float CheckingTimeBeforeToggle
    {
        get
        {
            return HandActionManager.Instance.CheckingTimeBeforeToggle;
        }
    }
    #endregion


    public RiggedHand Hand;
    public Transform toFaceCamera;
    public Vector3 LeftCalibrationPoint
    {
        get
        {
            HandActionManager.Instance.LeftCalibrationPoint = HandActionManager.Instance.mLeapHandController.transform.position - Vector3.right;
            return HandActionManager.Instance.LeftCalibrationPoint;
        }
    }
    public Vector3 RightCalibrationPoint
    {
        get
        {
            HandActionManager.Instance.RightCalibrationPoint = HandActionManager.Instance.mLeapHandController.transform.position + Vector3.right;

            return HandActionManager.Instance.RightCalibrationPoint;
        }
    }
    public Vector3 ForwardCalibrationPoint
    {
        get
        {
            HandActionManager.Instance.ForwardCalibrationPoint = HandActionManager.Instance.mLeapHandController.transform.position + Vector3.back;

            return HandActionManager.Instance.ForwardCalibrationPoint;
        }
    }
    public Vector3 BrackCalibrationPoint
    {
        get
        {
            HandActionManager.Instance.BrackCalibrationPoint = HandActionManager.Instance.mLeapHandController.transform.position - Vector3.back;

            return HandActionManager.Instance.BrackCalibrationPoint;
        }
    }



    public Vector3 GetHandVelocity()
    {
        if (Hand.hand_ != null)
        {
            Vector3 vector= transform.root.InverseTransformDirection(UnityVectorExtension.ToVector3(Hand.hand_.PalmVelocity))/transform.lossyScale.x;
           // Debug.Log(vector);
            return vector;
        }

        else return Vector3.zero;
    }

    public List<Vector3> CalibrationPositions = new List<Vector3>();
    public int MaxCount = 3;
  
    public float MinDistanceStep = 0.1f;
    public float MaxDistanceStep = 0.3f;
    public float distancestep;

    public float MinTimeStep = 0.2f;
    public float MaxTimeStep = 0.5f;

    public float TimeTemp = 0;

    public void UpdateCalibration()
    {
        if (Hand.hand_ != null && IsActive()&& !Grabed()&& !Pinched())
        {
            if (Hand.hand_.PalmVelocity.Magnitude >= 0.4f)
            {
                if (CalibrationPositions.Count > MaxCount)
                {
                    CalibrationPositions.Remove(CalibrationPositions[0]);

                  
                }

                Vector3 position = Hand.hand_.PalmPosition.ToVector3();
                if (CalibrationPositions.Count > 0)
                {
                     distancestep = Vector3.Distance(position, CalibrationPositions[CalibrationPositions.Count - 1]);



                    if (position != CalibrationPositions[CalibrationPositions.Count - 1]
                    && distancestep >= MinDistanceStep * transform.lossyScale.x
                     && distancestep <= MaxDistanceStep * transform.lossyScale.x)
                    {
                      
                        if (Time.time - TimeTemp >= MinTimeStep && Time.time - TimeTemp <= MaxTimeStep)
                        {
                          
                            CalibrationPositions.Add(position);
                        }
                        TimeTemp = Time.time;
                    }

                }
                else if (CalibrationPositions.Count == 0)
                {

                    CalibrationPositions.Add(position);

                }
            }
            else
            {
                
                CalibrationPositions.Clear();
            }
        }
        else
        {
            
            CalibrationPositions.Clear();
        }
    }


    public bool CalibrationMoveLeft()
    {
        if (CalibrationPositions.Count > 1)
        {
            float dis0 = Vector3.Distance(CalibrationPositions[0], LeftCalibrationPoint);
            float dis1 = Vector3.Distance(CalibrationPositions[1], LeftCalibrationPoint);
            if (dis0 > dis1) return true;
        }
        return false;
    }
    public bool CalibrationMoveRight()
    {
        if (CalibrationPositions.Count > 1)
        {
            float dis0 = Vector3.Distance(CalibrationPositions[0], LeftCalibrationPoint);
            float dis1 = Vector3.Distance(CalibrationPositions[1], LeftCalibrationPoint);
            if (dis0 < dis1) return true;
        }
        return false;
    }
    public bool CalibrationMoveUp()
    {
        if (CalibrationPositions.Count > 1)
        {
            float dis0 = Vector3.Distance(CalibrationPositions[0], HandActionManager.Instance.mLeapHandController.transform.position);
            float dis1 = Vector3.Distance(CalibrationPositions[1], HandActionManager.Instance.mLeapHandController.transform.position);
            if (dis0 < dis1) return true;
        }
        return false;
    }
    public bool CalibrationMoveDown()
    {
        if (CalibrationPositions.Count > 1)
        {
            float dis0 = Vector3.Distance(CalibrationPositions[0], HandActionManager.Instance.mLeapHandController.transform.position);
            float dis1 = Vector3.Distance(CalibrationPositions[1], HandActionManager.Instance.mLeapHandController.transform.position);
            if (dis0 > dis1) return true;
        }
        return false;
    }
    public bool CalibrationMoveForward()
    {
        if (CalibrationPositions.Count > 1)
        {
            float dis0 = Vector3.Distance(CalibrationPositions[0], ForwardCalibrationPoint);
            float dis1 = Vector3.Distance(CalibrationPositions[1], ForwardCalibrationPoint);
            if (dis0 < dis1) return true;
        }
        return false;
    }
    public bool CalibrationMoveBrack()
    {
        if (CalibrationPositions.Count > 1)
        {
            float dis0 = Vector3.Distance(CalibrationPositions[0], ForwardCalibrationPoint);
            float dis1 = Vector3.Distance(CalibrationPositions[1], ForwardCalibrationPoint);
            if (dis0 > dis1) return true;
        }
        return false;
    }

    public bool IsActive()
    {

        return Hand.gameObject.activeSelf;
    }

    /// <summary>
    /// 手是不是静止状态
    /// </summary>
    /// <returns></returns>
    public bool IsStationary()
    {
        if (Hand.hand_ != null && IsActive())
            return Hand.hand_.PalmVelocity.Magnitude < smallestVelocity &&CalibrationPositions.Count <= 1;//手心的速度大小 小于最小的速度
        else return false;
    }

    public bool IsMoveLeft()
    {

        if (Hand.hand_ != null && IsActive())
            return GetHandVelocity().x < -deltaVelocity && !IsStationary()&& Hand.hand_.GrabStrength==0;
        else return false;
    }

    public bool IsMoveRight()
    {
        if (Hand.hand_ != null && IsActive())
            return GetHandVelocity().x > deltaVelocity && !IsStationary() && Hand.hand_.GrabStrength == 0;
        else return false;
    }

    public bool IsMoveUp()
    {

        if (Hand.hand_ != null && IsActive())
            return GetHandVelocity().y > deltaVelocity && !IsStationary() && Hand.hand_.GrabStrength == 0;
        else return false;
    }

    public bool IsMoveDown()
    {
        if (Hand.hand_ != null && IsActive())
            return GetHandVelocity().y < -deltaVelocity && !IsStationary() && Hand.hand_.GrabStrength == 0;
        else return false;
    }
    public bool IsMoveForwad()
    {
        if (Hand.hand_ != null && IsActive())
            return GetHandVelocity().z > deltaVelocity && !IsStationary() && Hand.hand_.GrabStrength == 0;
        else return false;
    }

    public bool IsMoveBrack()
    {
        if (Hand.hand_ != null && IsActive())
            return GetHandVelocity().z < -deltaVelocity && !IsStationary() && Hand.hand_.GrabStrength == 0;
        else return false;
    }

    public bool IsFacingCameraForward()
    {
        return Vector3.Dot((HandActionManager.Instance.CameraRig.transform.position - toFaceCamera.position).normalized, toFaceCamera.up) >= HandActionManager.Instance.minAllowedDotProduct;
    }
    public bool IsFacingCameraBrack()
    {
        return Vector3.Dot((HandActionManager.Instance.CameraRig.transform.position - toFaceCamera.position).normalized, -toFaceCamera.up) >= HandActionManager.Instance.minAllowedDotProduct;
    }
    public bool IsFacingCameraRight()
    {
        return Vector3.Dot((HandActionManager.Instance.CameraRig.transform.position - toFaceCamera.position).normalized, toFaceCamera.right) >= HandActionManager.Instance.minAllowedDotProduct;
    }
    public bool IsFacingCameraLeft()
    {
        return Vector3.Dot((HandActionManager.Instance.CameraRig.transform.position - toFaceCamera.position).normalized, -toFaceCamera.right) >= HandActionManager.Instance.minAllowedDotProduct;
    }
    public bool IsFacingCameraUp()
    {
        return Vector3.Dot((HandActionManager.Instance.CameraRig.transform.position - toFaceCamera.position).normalized, toFaceCamera.forward) >= HandActionManager.Instance.minAllowedDotProduct;
    }
    public bool IsFacingCameraDown()
    {
        return Vector3.Dot((HandActionManager.Instance.CameraRig.transform.position - toFaceCamera.position).normalized, -toFaceCamera.forward) >= HandActionManager.Instance.minAllowedDotProduct;
    }
    public bool IsFacingController()
    {
        return Vector3.Dot((HandActionManager.Instance.mLeapHandController.transform.position - toFaceCamera.position).normalized, -toFaceCamera.forward) >= HandActionManager.Instance.minAllowedDotProduct;
    }

    public float IsFacingControllerValue()
    {
        return Vector3.Dot((HandActionManager.Instance.mLeapHandController.transform.position - toFaceCamera.position).normalized, -toFaceCamera.forward) ;
    }

    public float IsFacingControllerAngle()
    {
        return Vector3.Angle((  toFaceCamera.position- HandActionManager.Instance.mLeapHandController.transform.position), HandActionManager.Instance.mLeapHandController.transform.up);
    }

    public mDirection Stroke()
    {
        if (!Grabed() && !Pinched())
        {
            if (IsMoveLeft()) return mDirection.Left;
            if (IsMoveRight()) return mDirection.Right;
            if (IsMoveUp()) return mDirection.Up;
            if (IsMoveDown()) return mDirection.Down;
            if (IsMoveForwad()) return mDirection.Forwad;
            if (IsMoveBrack()) return mDirection.Brack;

        }
        return mDirection.Unknow;
    }


    public mDirection PalmFace()
    {
        if (IsActive())
        {

            if (IsFacingCameraForward()) return mDirection.Forwad;
            if (IsFacingCameraBrack()) return mDirection.Brack;
            if (IsFacingCameraRight()) return mDirection.Right;
            if (IsFacingCameraLeft()) return mDirection.Left;
            if (IsFacingCameraUp()) return mDirection.Up;
            if (IsFacingCameraDown()) return mDirection.Down;

        }

        return mDirection.Unknow;

    }

    public Vector3 PalmFacedirection()
    {
        return Hand.GetPalmDirection();
    }

    public bool Grabed()
    {
        if (IsActive())
        {
            int GrabedCount = 0;
            for (int i = 0; i < Hand.hand_.Fingers.Count; i++)
            {
                if (!Hand.hand_.Fingers[i].IsExtended)
                {
                    GrabedCount++;
                }
            }


            return Hand.hand_.GrabStrength >= gradStrength && Hand.hand_.GrabAngle >= 3
                && GrabedCount == Hand.hand_.Fingers.Count;
        }
        return false;
    }

    public bool Pinched()
    {
        if (IsActive())
        {
            return  Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
        }
        return false;
    }

    public bool Pinched(Finger.FingerType fingerType)
    {
        switch (fingerType)
        {
            case Finger.FingerType.TYPE_THUMB:
                return !Grabed() && !Hand.hand_.GetThumb().IsExtended && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
            //break;
            case Finger.FingerType.TYPE_INDEX:
                return !Grabed() && !Hand.hand_.GetIndex().IsExtended && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
            // break;
            case Finger.FingerType.TYPE_MIDDLE:
                return !Grabed() && !Hand.hand_.GetMiddle().IsExtended && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
            // break;
            case Finger.FingerType.TYPE_RING:
                return !Grabed() && !Hand.hand_.GetRing().IsExtended && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
            //break;
            case Finger.FingerType.TYPE_PINKY:
                return !Grabed() && !Hand.hand_.GetPinky().IsExtended && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
                //break;
        }
        return false;
    }

    public bool Good()
    {
        if (IsActive())
        {
            return !Grabed() && Hand.hand_.GetThumb().IsExtended
                  && !Hand.hand_.GetIndex().IsExtended
                  && !Hand.hand_.GetMiddle().IsExtended
                  && !Hand.hand_.GetRing().IsExtended
                  && !Hand.hand_.GetPinky().IsExtended
                  && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
        }
        return false;
    }

    public bool Yes()
    {
        if (IsActive())
        {
            return !Grabed() && !Hand.hand_.GetThumb().IsExtended
                  && Hand.hand_.GetIndex().IsExtended
                  && Hand.hand_.GetMiddle().IsExtended
                  && !Hand.hand_.GetRing().IsExtended
                  && !Hand.hand_.GetPinky().IsExtended
                  && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
        }
        return false;
    }

    public bool Ok()
    {
        if (IsActive())
        {
            return !Grabed() && Hand.hand_.GetThumb().IsExtended
                && Hand.hand_.GetIndex().IsExtended
                && !Hand.hand_.GetMiddle().IsExtended
                && !Hand.hand_.GetRing().IsExtended
                && !Hand.hand_.GetPinky().IsExtended
                && Hand.hand_.PinchStrength >= HandActionManager.Instance.PinchStrength;
        }
        return false;
    }




    // Use this for initialization
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {

        UpdateCalibration();
    }


}
