﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class HandActionManager : MonoBehaviour, IHandActionManager
{
    


    public static HandActionManager Instance { get; private set; }
    public LeapServiceProvider mLeapHandController;
    public MicroLight.MicroLightCameraRig CameraRig;

    public HandAction LeftHand;
    public HandAction RightHand;

    [HideInInspector]
    public Vector3 LeftCalibrationPoint;
    [HideInInspector]
    public Vector3 RightCalibrationPoint;
    [HideInInspector]
    public Vector3 ForwardCalibrationPoint;
    [HideInInspector]
    public Vector3 BrackCalibrationPoint;



    #region Logic variables
    [Header("[--Logic variables--]")]

    /// <summary>
    /// 捏指值
    /// </summary>
    [Range(0, 1)]
    public float PinchStrength = 0.8f;

    /// <summary>
    /// 指向值
    /// </summary>
    [Range(0, 1)]
    public float PalmFaceStrength = 0.6f;

    /// <summary>
    /// 指向值
    /// </summary>
    [Range(0, 1)]
    public float minAllowedDotProduct = 0.8f;
    [Range(0, 100)]
    [Tooltip("Delta degree to check 2 vectors same direction")]
    public float handForwardDegree = 30;//手向前的度数
    [Range(0, 1)]
    [Tooltip("Grab hand strength, one for close hand(grab)")]
    public float gradStrength = 0.9f;//抓住的力量
    [Range(0, 1)]
    [Tooltip("Velocity (m/s) move toward ")]
    public float smallestVelocity = 0.4f;//最小的速度
    [Range(0, 1)]
    [Tooltip("Velocity (m/s) move toward ")]
    public float deltaVelocity = 0.7f;//增量的速度
    [Range(0, 360)]
    [Tooltip("Angle when rotation vector is registered to change")]
    public float angleChangeRot = 4;//当旋转向量注册改变是的角度
    [Range(0, 500)]
    [Tooltip("Opposite direction 2 vectors")]
    public float diffAngle2Hands = 130;
    [Range(0, 500)]
    [Tooltip("Velocity opposite ")]
    public float diffAngle2Velocity = 150;
    [Range(0, 5)]
    [Tooltip("Time (secs) during the user behavior checker")]
    public float CheckingTimeBeforeToggle = 1.5f;//切换前检查时间
    [Range(1, 500)]
    [Tooltip("Time (secs) during the user behavior checker")]
    public float RotationStep = 100;//旋转的角度
    #endregion
    public float DistanceToController = 5;
	public bool CanStroke=true;
	public void OpenStroke()
	{
		CanStroke=true;
	}
	public void CloseStroke()
	{
		CanStroke=false;
	}
    // Use this for initialization
    void Start()
    {
        Instance = this;

        
       
        
    }
     
    // Update is called once per frame
    void Update()
    {




        if (LeftHand && RightHand)
        {
            
            if (LeftHand.IsActive() || RightHand.IsActive())
            {
                if (CanStroke)
                {
                    PinchStrokeInAction();

                    PinchStrokeOutAction();
                }


            }
            else
            {
               
                ReSetActions();
                
            }


        }







    }




    #region Logic State variables
    [Header("Logic States")]
    //State
    public float ActionintervalTime = 0.2f;


    #endregion

    #region ExportEvents
    [Header("PinchScale Events")]
  
    public bool PinchScaleGrabedStep0 = false;
    public bool PinchScaleGrabedStep1 = false;
    [HideInInspector]
    public float ScaleActionintervalTime = 0;
    public UnityEvent PinchScaleBegin;
    public UnityEvent PinchScaleEnd;
    [Header("PinchRotate Events")]
    public bool PinchRotateGrabedStep0 = false;
    public bool PinchRotateGrabedStep1 = false;
    [HideInInspector]
    public float RotateActionintervalTime = 0;
    public UnityEvent PinchRotateBegin;
    public UnityEvent PinchRotateEnd;
    [Header("PinchTranslate Events")]
    public bool PinchTranslateGrabedStep0 = false;
    public bool PinchTranslateGrabedStep1 = false;
    [HideInInspector]
    public float TranslateActionintervalTime = 0;
    public UnityEvent PinchTranslateBegin;
    public UnityEvent PinchTranslateEnd;


    [Header("HomeAction Events")]
    public bool HomeStep0 = false;
    public bool HomeStep1 = false;
    [HideInInspector]
    public float HomeActionintervalTime = 0;
    public UnityEvent HomeBegin;
    public UnityEvent HomeEnd;
    [Header("StrokeIn Events")]
    public bool StrokeInStep0 = false;
    public bool StrokeInStep1 = false;
    [HideInInspector]
    public float StrokeInActionintervalTime = 0;
    public UnityEvent StrokeInBegin;
    public UnityEvent StrokeInEnd;
    [Header("StrokeOut Events")]
    public bool StrokeOutStep0 = false;
    public bool StrokeOutStep1 = false;
    [HideInInspector]
    public float StrokeOutActionintervalTime = 0;
    [Range(0, 2)]
    public float StrokeOutPalmVelocityMagnitude = 0.9f;

    public UnityEvent StrokeOutBegin;
    public UnityEvent StrokeOutEnd;

    [Header("Move Events")]
    public bool MoveStep0 = false;
    public bool MoveStep1 = false;
    [HideInInspector]
    public float MoveActionintervalStartTime = 0;
    public float MoveActionintervalMinTime = 0.2f;
    public float MoveActionintervalAngle = 0.02f;

    [Range(0, 2)]
    public float MovePalmVelocityMagnitude = 0.9f;

    public UnityEvent MoveBegin;
    public UnityEvent MoveEnd;


    [Header("Reset Events")]

    public bool ResetStep0 = false;
    public bool ResetStep1 = false;
    [HideInInspector]
    public float ResetActionintervalTime = 0;
    public UnityEvent ResetBegin;
    public UnityEvent ResetEnd;
    [Header("UI Events")]
     
    public bool Show = false;
    public UnityEvent ShowUI;
    public UnityEvent HildeUI;
   



    #endregion


    #region Logic Comman

    public bool GrabedAll()
    {
        return LeftHand.Grabed() && RightHand.Grabed();
    }

    public bool SingleGrabed(Chirality chirality)
    {
        if (chirality == Chirality.Left)
        {
            return LeftHand.Grabed();
        }
        return RightHand.Grabed();
    }

    public bool LeftHand_SingleGrabed()
    {
        return LeftHand.Grabed() && !RightHand.Grabed();
    }

    public bool RightHand_SingleGrabed()
    {
        return !LeftHand.Grabed() && RightHand.Grabed();
    }

    public bool PinchAll()
    {
        return LeftHand.Pinched() && RightHand.Pinched();
    }

    public bool SinglePinch(Chirality chirality)
    {
        if (chirality == Chirality.Left)
        {
            return LeftHand.Pinched();
        }
        return RightHand.Pinched();
    }

    public bool LeftHand_SinglePinch()
    {
        return LeftHand.Pinched() && !RightHand.Pinched();
    }

    public bool RightHand_SinglePinch()
    {
        return !LeftHand.Pinched() && RightHand.Pinched();
    }

    public bool LeftHand_Single_MoveForwad_FaceCamera()
    {
        return (LeftHand.IsFacingCameraBrack() 
            || LeftHand.IsFacingCameraRight()
          ) && !LeftHand.IsStationary()

            && (LeftHand.IsMoveForwad())            
             && !LeftHand.Grabed()
             && !LeftHand.Pinched()

            && (LeftHand.CalibrationMoveForward()
            || LeftHand.CalibrationMoveLeft())
            ;
    }

    public bool RightHand_Single_MoveForwad_FaceCamera()
    {
        return (RightHand.IsFacingCameraBrack() 
            || RightHand.IsFacingCameraLeft() 
            ) && !RightHand.IsStationary()

            && (RightHand.IsMoveForwad())       
             && !RightHand.Grabed()
             && !RightHand.Pinched()

            && (RightHand.CalibrationMoveForward()
            || RightHand.CalibrationMoveRight())
             ;
    }

    public bool FaceUpAll()
    {
        return LeftHand.IsFacingCameraUp()
            && RightHand.IsFacingCameraUp()
            && !LeftHand.Grabed()
             && !LeftHand.Pinched()
             && !RightHand.Grabed()
             && !RightHand.Pinched();
    }

    public bool FaceDownAll()
    {
        return LeftHand.IsFacingCameraDown()
            && RightHand.IsFacingCameraDown()
            && !LeftHand.Grabed()
             && !LeftHand.Pinched()
             && !RightHand.Grabed()
             && !RightHand.Pinched();
    }

    public bool FaceForwadAll()
    {
        return LeftHand.IsFacingCameraForward()
            && RightHand.IsFacingCameraForward()
            && !LeftHand.Grabed()
             && !LeftHand.Pinched()
             && !RightHand.Grabed()
             && !RightHand.Pinched();
    }

    public bool FaceBrackAll()
    {
        return LeftHand.IsFacingCameraBrack()
            && RightHand.IsFacingCameraBrack()
            && !LeftHand.Grabed()
             && !LeftHand.Pinched()
             && !RightHand.Grabed()
             && !RightHand.Pinched();
    }

    public bool LeftHandFaceRight_RightFaceLeft()
    {
        return LeftHand.IsFacingCameraRight()
            && RightHand.IsFacingCameraLeft()
             && !LeftHand.Grabed()
             && !LeftHand.Pinched()
             && !RightHand.Grabed()
             && !RightHand.Pinched();
    }

    public bool LeftHandFaceRightAndMoveRightSingle()
    {
        return   LeftHand.IsMoveRight() && !LeftHand.IsStationary()
               && !LeftHand.Grabed()
             && !LeftHand.Pinched()
            && LeftHand.CalibrationMoveRight();
    }

    public bool RightHandFaceLeftAndMoveLeftSingle()
    {
        return  RightHand.IsMoveLeft() && !RightHand.IsStationary()
            && !RightHand.Grabed()
             && !RightHand.Pinched()
            && RightHand.CalibrationMoveLeft();
    }


    
    bool QuicklyRotate = false;
    


    
    
    

    public void PinchHomeAction()
    {
        bool Result = false;
        if (! HomeStep0)
        {
            Result = !LeftHand.IsFacingCameraUp() && !RightHand.IsFacingCameraUp();
            if (Result)
            {
                HomeActionintervalTime = Time.time;
                HomeStep0 = true;
            }
        }
        else
        {
            if (!HomeStep1)
            {
                Result = LeftHand.IsFacingCameraUp() && RightHand.IsFacingCameraUp();
                if (Result)
                {
                    if (Time.time - HomeActionintervalTime <= ActionintervalTime)
                    {
                        HomeStep1 = true;
                        if (HomeBegin != null)
                        {
                            HomeBegin.Invoke();
                        }
                    }
                    else
                    {
                        HomeStep0 = false;
                    }
                }
            }
            else
            {
                Result = !LeftHand.IsFacingCameraUp() && !RightHand.IsFacingCameraUp();
                if (Result)
                {
                    HomeStep0 = false;
                    HomeStep1 = false;
                    if (HomeEnd != null)
                    {
                        HomeEnd.Invoke();
                    }
                }
            }

        }



    }
    public void PinchStrokeInAction()
    {
        bool Result = false;
        if (!StrokeInStep0)
        {
            Result = !RightHand.Grabed()
               && !RightHand.Pinched()
             && RightHand.IsMoveRight();
           
            if (Result)
            {
                StrokeInActionintervalTime = Time.time;
                StrokeInStep0 = true;
            }
        }
        else
        {
            if (!StrokeInStep1)
            {
                if (Time.time - StrokeInActionintervalTime <= ActionintervalTime)
                {
                    Result = !RightHand.Grabed()
                      && !RightHand.Pinched()
                       && RightHand.IsMoveRight()
                  ;  
                    if (Result)
                {
                    
                        StrokeInStep1 = true;
                        if (StrokeInBegin != null)
                        {
                            StrokeInBegin.Invoke();
                            Debug.Log(this+"-------Next");
                        }
                        
                    }
                }
                else
                {
                    StrokeInStep0 = false;
                }
            }
            else
            {
                Result = RightHand.IsStationary()||RightHand.Grabed() || RightHand.Pinched()|| !RightHand.IsActive();
                if (Result)
                {
                    StrokeInStep0 = false;
                    StrokeInStep1 = false;
                    if (StrokeInEnd != null)
                    {
                        StrokeInEnd.Invoke();
                    }
                }
            }

        }



    }

    public void PinchStrokeOutAction()
    {
        bool Result = false;
        if (!StrokeInStep0)
        {
            Result = !RightHand.Grabed()
               && !RightHand.Pinched()
             && RightHand.IsMoveLeft();

            if (Result)
            {
                StrokeInActionintervalTime = Time.time;
                StrokeInStep0 = true;
            }
        }
        else
        {
            if (!StrokeInStep1)
            {
                if (Time.time - StrokeInActionintervalTime <= ActionintervalTime)
                {
                    Result = !RightHand.Grabed()
                      && !RightHand.Pinched()
                       && RightHand.IsMoveLeft()
                  ;
                    if (Result)
                    {

                        StrokeInStep1 = true;
                        if (StrokeInBegin != null)
                        {
                            //StrokeInBegin.Invoke();
                           // Debug.Log(this + "-------Next");
                        }

                    }
                }
                else
                {
                    StrokeInStep0 = false;
                }
            }
            else
            {
                Result = RightHand.IsStationary() || RightHand.Grabed() || RightHand.Pinched() || !RightHand.IsActive();
                if (Result)
                {
                    StrokeInStep0 = false;
                    StrokeInStep1 = false;
                    if (StrokeInEnd != null)
                    {
                        StrokeInEnd.Invoke();
                    }
                }
            }

        }



    }


    //public void PinchStrokeOutAction()
    //{
    //    bool Result = false;
    //    if (!StrokeOutStep0)
    //    {
    //        Result = !LeftHand.Grabed()
    //             && !LeftHand.Pinched()
    //              && LeftHand.IsMoveRight()
    //     ;     if (Result)
    //        {
    //            StrokeOutActionintervalTime = Time.time;
    //            StrokeOutStep0 = true;
    //        }
    //    }
    //    else
    //    {
    //        if (!StrokeOutStep1)
    //        {
    //            if (Time.time - StrokeOutActionintervalTime <= ActionintervalTime)
    //            {
    //                Result = !LeftHand.Grabed()
    //                 && !LeftHand.Pinched()
    //                  && LeftHand.IsMoveRight()
    //              ;      if (Result)
    //            {
                   
    //                    StrokeOutStep1 = true;
    //                    if (StrokeOutBegin != null)
    //                    {
    //                        StrokeOutBegin.Invoke();
    //                        Debug.Log(this+"----------Pre");
    //                    }
                         
    //                }
    //            }
    //            else
    //            {
    //                StrokeOutStep0 = false;
    //            }
    //        }
    //        else
    //        {
				//Result = LeftHand.IsStationary()||LeftHand.Grabed()||LeftHand.Pinched() || !LeftHand.IsActive();
    //            if (Result)
    //            {
    //                StrokeOutStep0 = false;
    //                StrokeOutStep1 = false;
    //                if (StrokeOutEnd != null)
    //                {
    //                    StrokeOutEnd.Invoke();
    //                }
    //            }
    //        }

    //    }



    //}
    public void PinchResetAction()
    {
        bool Result = false;
        if (!ResetStep0)
        {
            Result = !LeftHand.Good() && !RightHand.Good();
            if (Result)
            {
                ResetActionintervalTime = Time.time;
                ResetStep0 = true;
            }
        }
        else
        {
            if (!ResetStep1)
            {
                Result = LeftHand.Good() && RightHand.Good();
                if (Result)
                {
                    if (Time.time - ResetActionintervalTime <= ActionintervalTime)
                    {
                        ResetStep1 = true;
                        if (ResetBegin != null)
                        {
                            ResetBegin.Invoke();
                        }
                    }
                    else
                    {
                        ResetStep0 = false;
                    }
                }
            }
            else
            {
                Result = !LeftHand.Good() && !RightHand.Good();
                if (Result)
                {
                    ResetStep0 = false;
                    ResetStep1 = false;
                    if (ResetEnd != null)
                    {
                        ResetEnd.Invoke();
                    }
                }
            }

        }



    }

    private void ReSetActions()
    {
        if (PinchScaleGrabedStep1)
        {
            PinchScaleGrabedStep0 = false;
            PinchScaleGrabedStep1 = false;
            if (PinchScaleEnd != null)
            {
                PinchScaleEnd.Invoke();
            }
        }
        if (PinchRotateGrabedStep1)
        {
            PinchRotateGrabedStep0 = false;
            PinchRotateGrabedStep1 = false;
            if (PinchRotateEnd != null)
            {
                PinchRotateEnd.Invoke();
            }
        }
        if (PinchTranslateGrabedStep1)
        {
            PinchTranslateGrabedStep0 = false;
            PinchTranslateGrabedStep1 = false;
            if (PinchTranslateEnd != null)
            {
                PinchTranslateEnd.Invoke();
            }
        }
        if (MoveStep1)
        {
            MoveStep0 = false;
            MoveStep1 = false;
            if (MoveEnd != null)
            {
                MoveEnd.Invoke();
            }
        }
        if (HomeStep1)
        {
            HomeStep0 = false;
            HomeStep1 = false;
            if (HomeEnd != null)
            {
                HomeEnd.Invoke();
            }
        }
        if (StrokeInStep1)
        {
            StrokeInStep0 = false;
            StrokeInStep1 = false;
            if (StrokeInEnd != null)
            {
                StrokeInEnd.Invoke();
            }
        }
        if (StrokeOutStep1)
        {
            StrokeOutStep0 = false;
            StrokeOutStep1 = false;
            if (StrokeOutEnd != null)
            {
                StrokeOutEnd.Invoke();
            }
        }
        if (ResetStep1)
        {
            ResetStep0 = false;
            ResetStep1 = false;
            if (ResetEnd != null)
            {
                ResetEnd.Invoke();
            }
        }
    }


    int TimeCount = 500;
    //定义计时器
    float timer = 0;

    public bool MenuSwitch = true;

    

    //public void CheckActive()
    //{
    //    if((LeftHand.IsActive()||RightHand.IsActive()))
    //    {
    //        if (!Show)  //当前 手没有放下去
    //        {
    //            Show = true;
    //            if (MenuSwitch)
    //            {
    //                OperateGuidPanelShow();
    //                TextPanelShow();
    //                UIPanelShow();
    //            }
    //            //if (AniController.Instance.gestureAffectUI)
    //            //{
    //            //    UIPanelAni.Show();
    //            //    OperateGuidPanelAni.Show();
    //            //    InfoPanelAni.Show() ;
    //            //}
    //            //else
    //            //{
    //            //    UIPanelAni.Show();
    //            //}
    //        }
    //        else
    //        {
    //            //此时ui隐藏了  但不是由于手放下去导致ui隐藏
    //            //开始计时
                
    //            //if (MainController.Instance.CurrentState == State.Interaction && GetOperateState())
    //            {
    //                timer += Time.deltaTime;
    //                if (timer >= 1.5f)
    //                {
    //                    //显示ui
    //                    if (MenuSwitch)
    //                    {
    //                        OperateGuidPanelShow();
    //                        TextPanelShow();
    //                        UIPanelShow();
    //                    }
    //                    timer = 0;
    //                }
    //            }
    //           // else
    //            {
    //               // timer = 0;
    //            }
    //        }


    //    }
    //    else if(!LeftHand.IsActive() && !RightHand.IsActive()&&Show)
    //    {
    //        Show = false;
    //        if (MenuSwitch) {
    //            if (HildeUI != null) HildeUI.Invoke();

    //            TextPanelHide();
    //            OperateGuidPanelHide();
    //            UIPanelHide();
                
    //        }


    //    }
        

        
    //}



    #endregion

    //[Header("zhili")]
    //public AnimatorPlayer InfoPanelAni;
    //public AnimatorPlayer OperateGuidPanelAni;
    //public AnimatorPlayer UIPanelAni;

    //public bool infoPanelCanShow;
    //public bool operateGuidPanelCanShow;
    //public bool UIpanelCanShow;


    

    ///// <summary>
    ///// 文字语音介绍未结束不隐藏
    ///// </summary>
    //public void TextPanelHide() {
    //    if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "MainScene")
    //    {
    //        if (!TextWritter_Test.txtIsPlaying && AniController.Instance.gestureAffectUI)
    //            InfoPanelAni.Hide();//文字介绍未结束不隐藏
    //    }
    //}

    //public void OperateGuidPanelHide() {
    //    //Debug.Log(MainController.Instance.CurrentState != State.Interaction);
    //  //  if (AniController.Instance.gestureAffectUI)
    //    {
    //        //Debug.Log("Hide");
    //        OperateGuidPanelAni.Hide();
    //    }
        
    //}

    //public void UIPanelHide() {
    //    if (AniController.Instance.gestureAffectUI)
    //    {
    //        Debug.Log("hide");
    //        UIPanelAni.Hide();
    //    }
    //}




    //public void TextPanelShow()
    //{

    //    if (AniController.Instance.gestureAffectUI && infoPanelCanShow == true)
    //        InfoPanelAni.Show();

    //}

    //public void OperateGuidPanelShow()
    //{
    //    //Debug.Log(MainController.Instance.CurrentState != State.Interaction);
    //   // if (AniController.Instance.gestureAffectUI && operateGuidPanelCanShow == true && MainController.Instance.CurrentState == State.Interaction )
    //    {
    //        //Debug.Log("Hide");

    //        OperateGuidPanelAni.Show();
    //    }

    //}

    //public void UIPanelShow()
    //{
    //    if (AniController.Instance.gestureAffectUI && UIpanelCanShow == true)
    //    {
    //        UIPanelAni.Show();
    //    }
    //}

}
